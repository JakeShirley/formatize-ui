import React from "react";

import { withAPIAccess } from "../api/APIAccess";
import JobsAPI from "../api/JobsAPI";

export default withAPIAccess(class JobFinder extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dataPolling: null
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.jobGroupId !== this.props.jobGroupId) {
            this.action();
        }
    }

    async action() {
        const { api, paused, jobGroupId } = this.props;

        if (paused || typeof jobGroupId === "undefined")
            return;
        
        this.props.onPoll(await api.jobs.getGroup(jobGroupId));
    }

    componentDidMount() {
        if (this.state.dataPolling !== null)
            return;

        this.setState({
            dataPolling: setInterval(() => {
                this.action();
            }, this.props.interval || 1000)
        });
    }

    componentWillUnmount() {
        clearInterval(this.state.dataPolling);
    }

    render() {
        return this.props.children;
    }
});
