import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';

import red from '@material-ui/core/colors/red';

import SortableTableWithHeader from "../common/SortableTableWithHeader";
import { withAPIAccess } from "../api/APIAccess";

import moment from "moment";

const useStyles = makeStyles(theme => ({
    warning: {
        background: theme.palette.error.main,
        "& > *": {
            color: theme.palette.error.contrastText
        }
    },
}));

export default withAPIAccess(function JobsList(props) {
    const { api } = props;
    const [jobs, setJobs] = useState(null);

    const classes = useStyles();

    useEffect(() => {
        api.jobs.get().then(jobs => {
            let promises = jobs.map(x => x.output_filename ? api.files.getFileLink(x.output_filename) : new Promise((good, bad) => good(null)));

            Promise.all(promises).then(links => {
                setJobs(jobs.map((x, i) => ({...x, href: links[i]})));
            });
        });
    }, []);

    return [
    <SortableTableWithHeader
        loading={jobs === null}
        headers={[
            {
                id: "id",
                label: "Job ID"
            },
            {
                id: "status",
                label: "Status"
            },
            {
                id: "filename",
                label: "Output filename"
            },
            {
                id: "started_at",
                label: "Started at"
            },
            {
                id: "finished_at",
                label: "Finished at"
            },
            {
                id: "actions",
                label: "Actions"
            }]
        }
        data={jobs || []}
        paginate={true}
        
        paginationStartRows={10}
        paginationOptions={[5, 10, 20, 50]}
        row={(row, index) => {
            let isError = row.status === "NO_MAPPING_FOUND" || row.status === "VALIDATION_ERROR" || row.status === "ERROR";
            return <TableRow className={isError && classes.warning}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.status}</TableCell>
                <TableCell>{row.output_filename}</TableCell>
                <TableCell>{row.started_at ? moment(row.started_at).format("DD-MM-YYYY HH:mm:ss:SSS") : ""}</TableCell>
                <TableCell>{row.finished_at ? moment(row.finished_at).format("DD-MM-YYYY HH:mm:ss:SSS") : ""}</TableCell>
                <TableCell>
                    { row.href && <Button color="primary" href={row.href}>Download output</Button> }
                </TableCell>
            </TableRow>
        }}
        fullWidth
        title="Jobs"
    />];
});
