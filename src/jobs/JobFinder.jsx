import React from "react";

import { withAPIAccess } from "../api/APIAccess";

export default withAPIAccess(class JobFinder extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dataPolling: null
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.fileName !== this.props.fileName) {
            //this.action();
        }

        if (prevProps.files !== this.props.files) {
            this.componentDidMount();
        }
    }

    async action() {
        const { api, files, paused, onPoll } = this.props;

        if (paused)
            return;
        
        let p = files.map(file => api.jobs.query(file.name));
        let jobsList = await Promise.all(p);
        let jobs = jobsList.map(x => x.pop()).filter((x, i) => {
            return x && files[i]?.tempJobId != x.id;
        });

        if (jobs.length === files.length) {
            onPoll(jobs);

            clearInterval(this.state.dataPolling);
            this.setState({dataPolling: null});
        }
    }

    componentDidMount() {
        if (this.state.dataPolling !== null)
            return;

        this.setState({
            dataPolling: setInterval(() => {
                this.action();
            }, this.props.interval || 1000)
        });
    }

    componentWillUnmount() {
        clearInterval(this.dataPolling);
    }

    render() {
        return this.props.children;
    }
});
