import React from "react";

import { withAPIAccess } from "../api/APIAccess";

export default withAPIAccess(class JobPoll extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.jobs?.length != this.props.jobs?.length) {
            //this.action();
        }
    }

    async action() {
        const { api, jobs, paused } = this.props;

        if (paused)
            return;

        

        const promises = (jobs || []).map(j => {
            //if (j.status === "DONE")
              //  return j;

            

            return api.jobs.get(j.id, true);
        });
        if (promises.length === 0)
            return;
            
        this.props.onPoll(await Promise.all(promises).then(res => {
            return res.reduce((acc, val) => {
                acc = acc.concat(val);
                return acc;
            }, []);
        }));
    }

    componentDidMount() {
        this.dataPolling = setInterval(() => {
            this.action();
        }, this.props.interval || 1000);
    }

    componentWillUnmount() {
        clearInterval(this.dataPolling);
    }

    render() {
        return this.props.children || null;
    }
});
