import 'date-fns';
import 'typeface-roboto';
import './App.css';

import React, { useState, useEffect } from 'react';

import Container from "./Container.jsx";
import { BrowserRouter as Router, Route } from "react-router-dom";

import ClientsList from "./clients/ClientsList";
import Client from "./clients/Client";

import MappingList from "./mappings/MappingList";
import Mapping from "./mappings/Mapping";
import Files from "./files/Files";

import PipelineList from "./pipelines/PipelineList";
import Pipeline from "./pipelines/Pipeline";

import SchedulesList from "./time/SchedulesList";
import TimePeriod from "./clients/TimePeriod";

import JobsList from "./jobs/JobsList";

import APIKeys from "./apikeys/APIKeys";

import ValidatorList from "./validators/ValidatorList";
import Validator from "./validators/Validator";

import TransformEditor from "./transforms/TransformEditor.jsx";
import TransformList from "./transforms/TransformList.jsx";

import DateFnsUtils from '@date-io/date-fns';

import Login from "./login/Login.jsx";

import DataConversionTool from "./home/DataConversionTool";

import {
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';

import { APIAccess } from "./api/APIAccess";
import { UserAccess } from "./login/UserAccess";

import { withAuth } from '@okta/okta-react';
import { Security, ImplicitCallback, SecureRoute } from '@okta/okta-react';


import DefaultTheme from "./themes/DefaultTheme";

const config = {
  issuer: `${process.env.REACT_APP_OKTA_URL}/oauth2/default`,
  redirectUri: window.location.origin + '/implicit/callback',
  clientId: process.env.REACT_APP_OKTA_CLIENT_ID,
  pkce: true,
  onAuthRequired: ({ history }) => {
      history.push("/login");
  }
}

function App() {

  return (
    <div className="App">
      <DefaultTheme>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Router>
                <Security {...config}>
                <UserAccess>

                  <Container drawerWidth={"20%"}>
                      <APIAccess>
                          <Route path='/implicit/callback' component={ImplicitCallback}/>

                          <SecureRoute path="/" exact={true} component={DataConversionTool} />

                          <SecureRoute path="/pipelines" exact={true} component={PipelineList} />
                          <SecureRoute path="/pipeline/:id" exact={true} component={Pipeline} />

                          <SecureRoute path="/mappings" exact={true} component={MappingList} />
                          <SecureRoute path="/mapping/:id" exact={true} component={Mapping} />

                          <SecureRoute path="/transforms" exact={true} component={TransformList} />
                          <SecureRoute path="/transform/:id" exact={true} component={TransformEditor} />

                          <SecureRoute path="/files" exact={true} component={Files} />

                          <SecureRoute path="/clients" exact={true} component={ClientsList} />
                          <SecureRoute path="/client/:id" exact={true} component={Client} />
                          <SecureRoute path="/client/:id/time-period/:timePeriodId" exact={true} component={TimePeriod} />

                          <SecureRoute path="/schedules" exact={true} component={SchedulesList} />

                          <SecureRoute path="/jobs" exact={true} component={JobsList} />
                          <SecureRoute path="/api-keys" exact={true} component={APIKeys} />

                          <SecureRoute path="/validators" exact={true} component={ValidatorList} />
                          <SecureRoute path="/validator/:id" exact={true} component={Validator} />

                          <Route path="/login" exact={true} component={Login} />

                          <SecureRoute
                            path="/logout"
                            render={withAuth(props => {
                                props.auth.getIdToken().then(idToken => {
                                    props.auth
                                        .logout("/login")
                                        .catch(e => console.log(e))
                                        .then(e => {
                                            //window.('shutdown');
                                            //window.location.href = `${config.issuer}/v1/logout?id_token_hint=${idToken}&post_logout_redirect_uri=${config.redirect_uri}`;
                                        });
                                });

                                //window.location.href = `${issuer}/v1/logout?id_token_hint=${idToken}&post_logout_redirect_uri=${redirectUri}`;

                                return null;
                            })}
                        />
                      </APIAccess>
                  </Container>
                  </UserAccess>
                 </Security>
            </Router>
        </MuiPickersUtilsProvider>
        </DefaultTheme>
    </div>
  );
}

export default App;
