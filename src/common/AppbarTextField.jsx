import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';

import grey from '@material-ui/core/colors/grey';

const textBoxColor = grey[500];

const styles = theme => ({
    textfield: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(textBoxColor, 0.15),
        '&:hover': {
            backgroundColor: fade(textBoxColor, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: 'auto',
        },
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
    },
    inputInput: {
        paddingTop: theme.spacing(),
        paddingRight: theme.spacing(),
        paddingBottom: theme.spacing(),
        paddingLeft: theme.spacing(1.5),
        transition: theme.transitions.create('width'),
        width: '100%',
        "&::-webkit-outer-spin-button": {
            "-webkit-appearance": "none",
            margin: 0
        },
        [theme.breakpoints.up('sm')]: {
        //width: 120,
            '&:focus': {
                //width: 200,
            },
        }
    }
});

class AppbarTextField extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { classes } = this.props;

        return (<div className={classes.textfield}>
            <InputBase
                {...this.props}
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                }}
            />
        </div>);
    }
}

export default withStyles(styles)(AppbarTextField);
