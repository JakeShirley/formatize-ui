import React, { Suspense } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import TopBar from "./TopBar.jsx";
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles(theme => ({
    root: {
        margin: 10
    },
    content: {
        padding: theme.spacing(2, 4)
    }
}));

export default function Section(props) {
    const classes = useStyles();

    return <div className={classes.root}>
        <Paper {...props}>
            {props.children}
        </Paper>
    </div>
}
