import React, { useEffect, useRef, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import ListItemLink from "../common/ListItemLink.jsx";
import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import AppbarTextField from './AppbarTextField.jsx';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import Box from '@material-ui/core/Box';
import $ from "jquery";

import { withAPIAccess } from "../api/APIAccess.jsx";

function trunc(s, len) {
    return (s.length > len) ? s.substr(0, len - 1) + "..." : s;
}

const useStyles = makeStyles(theme => ({
    list: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));

const FILTERS = [{
    name: "Your clients",
    type: "YOURS"
}, {
    name: "All clients",
    type: "ALL"
}];

function MessageFilters(props) {
    const [anchorEl, setAnchorEl] = useState(null);
    const clientFilter = props.clientFilter;

    const menuItemClick = filter => e => {
        props.filterSelected(filter);
        setAnchorEl(null);
    };

    return <Box display="flex" flexDirection="row">
        <Box flexGrow={1} position="relative">
            <AppbarTextField name="search" placeholder="Search..." value={props.searchValue} onChange={e => props.searchChange(e.target.value)}/>
        </Box>
        <Box width="190px">
            <Button onClick={e => setAnchorEl(e.currentTarget)}>
                {clientFilter.name}
            </Button>
            <Menu
                open={Boolean(anchorEl)}
                onClose={e => setAnchorEl(null)}
                anchorEl={anchorEl}
                value={clientFilter.type}
                onChange={e => console.log(e.target)}
            >
                {FILTERS.map(filter => (
                    <MenuItem selected={filter.type == clientFilter.type} onClick={menuItemClick(filter)} key={filter.type} value={filter.type}>
                        {filter.name}
                    </MenuItem>
                ))}
            </Menu>
        </Box>
    </Box>
}

export default withAPIAccess(function (props) {
    const { api } = props;

    const classes = useStyles();
    const { clients, clientFunc, maxTextLength, authToken, oktaId, load } = props;

    const [clientFilter, setClientFilter] = useState({ owner: FILTERS[0], search: "" });
    const [searchValue, setSearchValue] = useState("");
    const [loading, setLoading] = useState(load);

    useEffect(() => {
        if (!authToken || !load)
            return;

        api.clients.getClients("", clientFilter.owner.type == "YOURS" ? oktaId : "").then(clients => {
            if (props.onClientsLoaded)
                props.onClientsLoaded(clients);

            setLoading(false);
        });
    }, [load, authToken, searchValue, clientFilter]);

    if (loading) {
        return <div>Loading...</div>
    }

    return <><MessageFilters searchValue={searchValue} clientFilter={clientFilter.owner} filterSelected={filter => {
        setClientFilter({ ...clientFilter, owner: filter });
        //setClientsReady(false);
    }} searchChange={e => setClientFilter({ ...clientFilter, search: e })} searchValue={clientFilter.search}></MessageFilters>
    <List className={classes.list}>
        {clients.map(client => {
            let res = clientFunc(client);

            let text = trunc(res.text, maxTextLength || 50);

            return <ListItemLink href={res.href} alignItems="flex-start" selected={client.Id == props.selectedId}>
                <ListItemAvatar>
                    <Avatar alt={client.name} src={res.avatar} />
                </ListItemAvatar>

                <ListItemText
                    primary={client.name}
                    secondary={
                        <React.Fragment>
                            {text}
                        </React.Fragment>
                    }/>
                <Badge badgeContent={res.updates} color="secondary"></Badge>
            </ListItemLink>
        })}
    </List>{props.children}</>
});
