// src/Home.js

import React, { Component } from 'react';
import { withAuth } from '@okta/okta-react';
import OktaAuth from '@okta/okta-auth-js';
import { withStyles } from '@material-ui/styles';

import Grid from "@material-ui/core/Grid";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import DateFnsUtils from '@date-io/date-fns';
import FormLabel from '@material-ui/core/FormLabel';
import SectionBase from "../common/SectionBase";
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { ValidatorForm, TextValidator, SelectValidator } from 'react-material-ui-form-validator';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';

const styles = theme => ({
    root: {
        padding: "10px"
    },
    alert: {
        padding: "10px",
        marginTop: "10px",
        marginBottom: "10px"
    },
    error: {
        background: red[400],
        color: "white"
    },
    success: {
        background: green[400],
        color: "white"
    },
    buttons: {
        paddingTop: "10px",
    }
});

export default withStyles(styles)(withAuth(class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { authenticated: null, username: "", password: "", userinfo: null, error: null, message: null };
        this.checkAuthentication = this.checkAuthentication.bind(this);
        this.checkAuthentication();
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);

        this.oktaAuth = new OktaAuth({ url: process.env.REACT_APP_OKTA_URL });
    }

    async checkAuthentication() {
        const authenticated = await this.props.auth.isAuthenticated();
        if (authenticated !== this.state.authenticated) {
            const userinfo = await this.props.auth.getUser();

            this.setState({ authenticated, userinfo });
        }
    }

    componentDidUpdate() {
        this.checkAuthentication();
    }

    async login() {
    // Redirect to '/' after login
        this.props.auth.login('/');
    }

    async logout() {
    // Redirect to '/' after logout
        this.props.auth.logout('/');
    }

    changeTextField() {
        return e => {
            this.setState({ [e.target.name]: e.target.value });
        }
    }

    handleSubmit(e) {
        e.preventDefault();

        this.oktaAuth.signIn({
            username: this.state.username,
            password: this.state.password
        }).then(res => this.setState({
            sessionToken: res.sessionToken,
            error: null
        })).catch(err => {
            console.log('Found an error', err);
            this.setState({ message: null, error: err });
        });
    }

    forgotPassword() {
        return e => {
            const REACT_APP_SIMPLYSHARE_API = process.env.REACT_APP_SIMPLYSHARE_API || process.env.REACT_APP_CLIENTS_API;
            fetch(REACT_APP_SIMPLYSHARE_API + "/users/password-recovery", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ username: this.state.username, returnUrl: "/test" }),
            }).catch(er => {
                console.log("Error Found")
                console.error(er);
            }).then(res => {
                console.log(res);
                this.setState({ error: null, message: "A password reset email has been sent to " + this.state.username });
            });
        }
    }

    render() {
        if (this.state.authenticated === null) return null;

        const { classes } = this.props;
        const { error, message } = this.state;

        console.log(classes);

        if (this.state.sessionToken) {
            this.props.auth.redirect({ sessionToken: this.state.sessionToken });
            return null;
        }

        const Alert = props => {
            if (props.visible) {
                return <Paper className={classes.alert + " " + classes[props.type]}>
                    {props.children}
                </Paper>
            } else {
                return null;
            }
        }

        if (!this.state.authenticated) {
            return <Grid container
                direction="row"
                justify="center"
                alignItems="center">
                <Grid item xs={6}>
                    <Paper className={classes.root}>
                        <Typography variant="h4">Sign in</Typography>
                        <ValidatorForm
                            onSubmit={this.handleSubmit.bind(this)}>
                            <TextValidator name="username" label="Email" value={this.state.username} fullWidth onChange={this.changeTextField()}
                                validators={['required']}
                                errorMessages={['This field is required!']}/>
                            <TextValidator name="password" type="password" label="Password" value={this.state.password} fullWidth onChange={this.changeTextField()}
                                validators={['required']}
                                errorMessages={['This field is required!']}/>

                            <Alert type="error" visible={error}>{error && error.errorCode == "E0000004" ? "Invalid username or password" : "Unknown error"}</Alert>
                            <Alert type="success" visible={message}>{message}</Alert>

                            <ButtonGroup className={classes.buttons} fullWidth color="primary" aria-label="small outlined button group">
                                <Button type="submit">Login</Button>
                                <Button onClick={this.forgotPassword()}>Forgot my password</Button>
                            </ButtonGroup>
                        </ValidatorForm>
                    </Paper>
                </Grid>
            </Grid>
        } else {
            return <Button color="inherit" onClick={this.logout}>Logout</Button>
        }

        /*return this.state.authenticated ?
            <button onClick={this.logout}>Logout {JSON.stringify(this.state.userinfo.organization)}</button> :
            <button onClick={this.login}>Login</button>;*/
    }
}));
