import React, { useState, useEffect } from "react";

import { withAuth } from '@okta/okta-react';
import jwt_decode from "jwt-decode";

const UserContext = React.createContext();

function isAdmin(user) {
    return user && user.groups.indexOf("Administrators") !== -1;
}

export const UserAccess = withAuth((props) => {
    const { auth } = props;

    const [authenticated, setAuthenticated] = useState(null);
    const [profile, setProfile] = useState(null);
    const [token, setToken] = useState(null);

    auth.isAuthenticated().then(a => {
        if (a !== authenticated) {
            setAuthenticated(a);
            Promise.all([
            auth.getAccessToken(),
            auth.getUser()])
            .then(([t, user]) => {
                if (token !== t) {
                    setProfile(a ? {...user, ...jwt_decode(t)} : null);
                    setToken(a ? t : null);
                }
            });
        }
    });

    return <UserContext.Provider value={{
        auth,
        profile,
        token,
    }}>
        {props.children}
    </UserContext.Provider>
});

export default function withUser(Component) {
    return props => {
        return <UserContext.Consumer>
            { (user) => <Component {...props} user={user} />}
        </UserContext.Consumer>
    }
}