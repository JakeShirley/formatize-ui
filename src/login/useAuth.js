import { useState, useEffect } from 'react';
import jwt_decode from "jwt-decode";

function isAdmin(user) {
    return user && user.groups.indexOf("Administrators") !== -1;
}

let unmounted = false;

export default function useAuth(auth) {
    const [authenticated, setAuthenticated] = useState(null);
    const [user, setUser] = useState(null);
    const [token, setToken] = useState(null);
    const [userInfo, setUserInfo] = useState(null);

    useEffect(() => {
        auth.isAuthenticated().then(isAuthenticated => {
            if (isAuthenticated !== authenticated) {
                setAuthenticated(isAuthenticated);
            }
        });

        return () => {
            unmounted = true;
        }
    });

    useEffect(() => {
        if (authenticated) {
            Promise.all([auth.getAccessToken(), auth.getUser()]).then(res => {
                try {
                    let user = jwt_decode(res[0]);

                    setUser(user);
                    setUserInfo(res[1]);
                    setToken(res[0]);
                } catch (e) {
                    console.log(e);
                }
            });
        } else {
            setToken(null);
            setUser(null);
            setUserInfo(null);
        }
    }, [authenticated]);

    return [authenticated, token, user, isAdmin(user), user ? user.organization || "" : "", userInfo];
}
