export default class SchedulesAPI {
    constructor(service, api) {
        this.service = service;

        this.api = api;
    }

    create(name, start, end, duration, repeatEvery, repeatPeriod, recurring) {
        return this.service.post("/schedules", {
            body: JSON.stringify({
                name,
                start,
                end,
                duration,
                repeatEvery,
                repeatPeriod,
                recurring
            })
        }).then(res => res.json());
    }

    get(scheduleId) {
        if (typeof scheduleId == "undefined") {
            return this.service.get("/schedules").then(res => res.json());
        } else {
            return this.service.get("/schedules/" + scheduleId).then(res => res.json());
        }
    }

    delete(scheduleId) {
        return this.service.delete("/schedules/" + scheduleId).then(res => res.json());
    }

    deleteTimePeriod(timePeriodId) {
        return this.service.delete("/time-periods/" + timePeriodId).then(res => res.json());
    }
}
