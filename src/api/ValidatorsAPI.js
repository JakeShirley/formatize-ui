import APIService from "./APIService";

const fetch = require("node-fetch");
const convert = require('xml-js');

export default class ValidatorsAPI {
    constructor(service, api) {
        this.service = service;

        this.api = api;
    }

    async create(name, description) {
        return this.service.post("/validators", {
            body: JSON.stringify({
                name,
                description
            })
        }).then(res => res.json());
    }

    async get(id) {
        if (typeof id === "undefined") {
            return this.service.get("/validators").then(res => res.json());
        } else {
            return this.service.get("/validators/" + id).then(res => res.json());
        }
    }

    async update(id, params = {}) {
        return this.service.patch("/validators/" + id, {
            body: JSON.stringify(params)
        }).then(res => res.json());
    }

    async delete(id) {
        return this.service.delete("/validators/" + id);
    }
}
