import React from "react";
import APIClient from "./APIClient.js";
import { Security, ImplicitCallback, SecureRoute, withAuth, Auth } from '@okta/okta-react';

const API_URI = process.env.REACT_APP_API_URI;

const APIContext = React.createContext();

function withAPIAccess(Component) {
    return props => {
        return <APIContext.Consumer>
            { (api) => <Component {...props} api={api} /> }
           </APIContext.Consumer>
    }
}

const APIAccess = withAuth(function (props) {
    const [apiClient, setAPIClient] = React.useState(new APIClient({ oktaAuth: props.auth, uri: API_URI } ));

    return (
      <APIContext.Provider value={apiClient}>
          {props.children}
      </APIContext.Provider>
    );
});

export { APIAccess, withAPIAccess };
