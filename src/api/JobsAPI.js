import APIService from "./APIService";

const fetch = require("node-fetch");
const convert = require('xml-js');

export default class JobsAPI {
    constructor(service, api) {
        this.service = service;

        this.api = api;
    }

    create(filename, mapping, group) {
        return this.service.post("/jobs", {
            body: JSON.stringify({
                filename,
                mapping: mapping || null,
                group
            })
        }).then(res => res.json());
    }

    createGroup(outputName, jobCount) {
        return this.service.post("/job-groups", {
            body: JSON.stringify({
                outputName,
                jobCount
            })
        }).then(res => res.json());
    }

    get(jobId, includeStages) {
        if (typeof jobId == "undefined") {
            return this.service.get("/jobs").then(res => res.json());
        } else {
            return this.service.get("/jobs/" + jobId + (includeStages ? "?includeStages=true" : "")).then(res => res.json());
        }
    }

    getGroup(groupId) {
        return this.service.get(`/job-groups/${groupId}`).then(res => res.json());
    }

    query(fileName) {
        return this.service.get("/jobs?file-name=" + fileName).then(res => res.json());
    }
}
