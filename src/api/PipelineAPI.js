import APIService from "./APIService";

const fetch = require("node-fetch");
const convert = require('xml-js');

export default class PipelineAPI {
    constructor(service, api) {
        this.service = service;

        this.api = api;
    }

    async create(name) {
        return this.service.post("/pipelines", {
            body: JSON.stringify({
                name
            })
        }).then(res => res.json());
    }

    async get(id) {
        if (typeof id === "undefined") {
            return this.service.get("/pipelines").then(res => res.json());
        } else {
            return this.service.get("/pipelines/" + id).then(res => res.json());
        }
    }

    async delete(id) {
        return this.service.delete("/pipelines/" + id);
    }

    async addDataSource(pipelineId, id, name, type) {
        return this.service.post("/pipelines/" + pipelineId + "/data-source", {
            body: JSON.stringify({
                id,
                name,
                type
            })
        }).then(res => res.json());
    }

    async updateDataSource(pipelineId, id, newId, name, type) {
        return this.service.patch("/pipelines/" + pipelineId + "/data-source/" + id, {
            body: JSON.stringify({
                id: newId,
                name,
                type
            })
        }).then(res => res.json());
    }

    async addMapping(pipelineId, mappingId, dataSourceId) {
        return this.service.post("/pipelines/" + pipelineId + "/mapping", {
            body: JSON.stringify({
                mapping: mappingId,
                dataSource: dataSourceId
            })
        }).then(res => res.json());
    }

    async deleteMapping(pipelineId, mappingId) {
        return this.service.delete("/pipelines/" + pipelineId + "/mapping/" + mappingId).then(res => res.json());
    }
}
