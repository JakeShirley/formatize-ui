export default class TransformsAPI {
    constructor(service, api) {
        this.service = service;

        this.api = api;
    }

    create(name, label, func) {
        return this.service.post("/transforms", {
            body: JSON.stringify({
                name,
                label,
                func
            })
        }).then(res => res.json());
    }

    get(id) {
        if (typeof id == "undefined") {
            return this.service.get("/transforms").then(res => res.json());
        } else {
            return this.service.get("/transforms/" + id).then(res => res.json());
        }
    }

    delete(id) {
        return this.service.delete("/transforms/" + id);
    }

    getAll() {
        return this.service.get("/transforms").then(res => res.json());
    }

    update(id, config) {
        return this.service.patch("/transforms/" + id, {
            body: JSON.stringify(config)
        }).then(res => res.json());
    }

    getRecommendations(columnHeader) {
        return this.service.get("/recommendations?header=" + encodeURIComponent(columnHeader)).then(res => res.json()).then(jsonArray => {
            jsonArray = jsonArray.map(json => {
                json.params = JSON.parse(json.params_json);
                json.transform.params = JSON.parse(json.transform.params_json);
                delete json.params_json;
                delete json.transform.params_json;

                return json;
            });
            
            return jsonArray;
        });
    }
}
