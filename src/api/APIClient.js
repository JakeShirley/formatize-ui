import APIService from "./APIService.js";

import Files from "./FileAPI.js";
import Mappings from "./MappingAPI.js";
import Jobs from "./JobsAPI.js";
import Clients from "./ClientAPI.js";
import Pipelines from "./PipelineAPI.js";
import Schedules from "./ScheduleAPI.js";
import Keys from "./KeysAPI.js";
import Validators from "./ValidatorsAPI.js";
import Transforms from "./TransformsAPI.js";

export default class APIClient {
    constructor({ oktaAuth, uri }) {
        this.oktaAuth = oktaAuth;

        this.service = new APIService(oktaAuth, uri);

        this.files = new Files(this.service, this);
        this.mappings = new Mappings(this.service, this);
        this.jobs = new Jobs(this.service, this);
        this.clients = new Clients(this.service, this);
        this.pipelines = new Pipelines(this.service, this);
        this.schedules = new Schedules(this.service, this);
        this.keys = new Keys(this.service, this);
        this.validators = new Validators(this.service, this);
        this.transforms = new Transforms(this.service, this);
    }

    async isAdmin() {
        let user = await this.service.getUser();
        return user.groups.includes("Administrators");
    }
}
