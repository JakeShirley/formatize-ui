import APIService from "./APIService";

const fetch = require("node-fetch");
const convert = require('xml-js');

export default class MappingAPI {
    constructor(service, api) {
        this.service = service;

        this.api = api;
    }

    create(id, signature) {
        return this.service.post("/mappings", {
            body: JSON.stringify({
                id,
                signature
            })
        }).then(res => res.json());
    }

    get(id) {
        if (typeof id == "undefined") {
            return this.service.get("/mappings").then(res => res.json());
        } else {
            return this.service.get("/mappings/" + id).then(res => res.json());
        }
    }

    delete(id) {
        return this.service.delete("/mappings/" + id);
    }

    getAll() {
        return this.service.get("/mappings").then(res => res.json());
    }

    update(id, config) {
        return this.service.patch("/mappings/" + id, {
            body: JSON.stringify(config)
        }).then(res => res.json());
    }

    addStage(mapping, name, description) {
        return this.service.post("/mapping-stages", {
            body: JSON.stringify({
                mapping,
                name,
                description
            })
        }).then(res => res.json());
    }

    removeStage(id) {
        return this.service.delete("/mapping-stages/" + id).then(res => res.json());
    }

    assignTransform(mappingStage, transformId, order, params) {
        return this.service.post("/transforms/instances", {
            body: JSON.stringify({
                mappingStage,
                name: transformId,
                order,
                params
            })
        }).then(res => res.json()).then(json => {
            json.params = JSON.parse(json.params_json);
            delete json.params_json;
            return json;
        });
    }

    setTransformOrder(transformId, order) {
        return this.updateTransform(transformId, { order });
    }

    updateTransform(transformId, update = {}) {
        return this.service.patch("/transforms/instances/" + transformId, {
            body: JSON.stringify(update)
        });
    }

    unassignTransformInstance(mappingStage, id) {
        return this.service.delete("/transforms/instances/" + id);
    }

    addColumnTransform(mappingStage, name) {
        return this.assignTransform(mappingStage, "add-column", {
            column: name
        });
    }

    getTransforms() {
        return this.service.get("/transforms").then(res => res.json()).then(json => {
            return json.map(x => {
                x.params = JSON.parse(x.params_json);
                delete x.params_json;
                return x;
            });
        });
    }

    getAssignedTransforms(mappingStage) {
        return this.service.get("/transforms/instances/" + mappingStage).then(res => res.json()).then(json => {
            return json.map(x => {
                x.params = JSON.parse(x.params_json);
                delete x.params_json;
                return x;
            }).sort((a, b) => a.order - b.order);
        });
    }
}
