import APIService from "./APIService";

const fetch = require("node-fetch");

export default class ClientAPI {
    constructor(service, api) {
        this.service = service;

        this.api = api;
    }

    async create(name, status) {
        return this.service.post("/clients", {
            body: JSON.stringify({
                name, status
            })
        }).then(res => res.json());
    }

    async get(id) {
        if (typeof id === "undefined") {
            return this.service.get("/clients").then(res => res.json());
        } else {
            return this.service.get("/clients/" + id).then(res => res.json());
        }
    }

    async delete(id) {
        return this.service.delete("/clients/" + id);
    }

    async getTimePeriods(clientId) {
        return this.service.get("/clients/" + clientId + "/time-periods").then(res => res.json());
    }

    async getTimePeriod(clientId, timePeriodId) {
        return this.service.get("/clients/" + clientId + "/time-periods/" + timePeriodId).then(res => res.json());
    }

    async getTimePeriodDataSources(clientId, timePeriodId) {
        return this.service.get("/clients/" + clientId + "/time-periods/" + timePeriodId + "/data-sources").then(res => res.json());
    }

    async addPipeline(clientId, pipelineId, scheduleId) {
        return this.service.post("/clients/" + clientId + "/pipelines", {
            body: JSON.stringify({
                pipeline: pipelineId,
                schedule: scheduleId
            })
        }).then(res => res.json());
    }

    async removePipeline(clientId, pipelineId) {
        return this.service.delete("/clients/" + clientId + "/pipelines/" + pipelineId).then(res => res.json());
    }
}
