import React, { useState, useEffect } from "react";

import SortableTableWithHeader from "../common/SortableTableWithHeader";

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';

import { withAPIAccess } from "../api/APIAccess";

export default withAPIAccess(function TimePeriodsTable(props) {
    const { api, client } = props;

    const [timePeriods, setTimePeriods] = useState(client.time_periods || []);

    useEffect(() => {
        setTimePeriods(client.time_periods || []);
    }, [client]);

    const onDialogAdd = values => {

    };

    const deleteTimePeriodInstance = timePeriodInstance => e => {
        api.schedules.deleteTimePeriod(timePeriodInstance).then(e => {
            console.log(timePeriodInstance);
            setTimePeriods(timePeriods.filter(x => x.id !== timePeriodInstance));
        });
    }

    return <SortableTableWithHeader
        headers={[
        {
            id: "start",
            label: "Start period"
        },
        {
            id: "end",
            label: "End period"
        },
        {
            id: "actions",
            label: "Actions"
        },]}
        data={timePeriods}
        row={(row, index) => {
            return <TableRow>
                <TableCell>{row.start}</TableCell>
                <TableCell>{row.end}</TableCell>
                <TableCell>
                    <Button href={"/client/" + client.id + "/time-period/" + row.id}>View</Button>
                    <Button onClick={deleteTimePeriodInstance(row.id)}>Delete</Button>
                </TableCell>
            </TableRow>
        }}
        paginate={true}
        paginationStartRows={5}
        paginationOptions={[5, 10, 20, 50]}
        title="Upload periods"
    />
});
