import React, { useState, useEffect } from "react";

import FileCardManager from "../files/FileCardManager";

import AppToolbar from "../common/AppToolbar";

import withUser from "../login/UserAccess";
import { withAPIAccess } from "../api/APIAccess";

export default withAPIAccess(withUser(function TimePeriod(props) {
    const [client, setClient] = useState(null);
    const [timePeriod, setTimePeriod] = useState(null);

    const [uploadables, setUploadables] = useState(null);

    const { api, user } = props;
    const { id, timePeriodId } = props.match.params;

    const isAdmin = user?.profile?.groups?.includes("Administrators");

    useEffect(() => {
        api.clients.getTimePeriod(id, timePeriodId).then(timePeriod => {
            api.clients.getTimePeriodDataSources(id, timePeriodId).then(ups => {
                api.clients.get(id).then(client => {
                    setTimePeriod(timePeriod);
                    setUploadables(ups);
                    setClient(client);
                });
            });
        });
    }, [id]);

    if (!client)
        return null;

    return [<AppToolbar title={"Time Period for '" + timePeriod.schedule.name + "'"} />,
            <FileCardManager isAdmin={isAdmin} client={client} types={uploadables} timePeriodId={timePeriodId}/>
    ];
}));