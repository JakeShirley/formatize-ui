import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Box from '@material-ui/core/Box';

import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

import Select from '@material-ui/core/Select';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import SortableTableCRUD from "../common/SortableTableCRUD";

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';

import { withAPIAccess } from "../api/APIAccess";

export default withAPIAccess(function PipelineTable(props) {
    const [availablePipelines, setAvailablePipelines] = useState([]);
    const [availableTimePeriods, setAvailableTimePeriods] = useState([]);

    const { api, client } = props;

    useEffect(() => {
        Promise.all([
            api.pipelines.get(),
            api.schedules.get()
        ]).then(([pipelines, timePeriods]) => {
            setAvailablePipelines(pipelines);
            setAvailableTimePeriods(timePeriods);
        });
    }, [client.id]);

    const onDialogAdd = values => {
        console.log("OKOK");
        api.clients.addPipeline(client.id, values.id, values.schedule).then(pipeline => {
            props.onUpdate();
        }).catch(res => {
            console.log(res);
        });
    }

    const deletePipeline = id => e => {
        console.log("OKOK");
        api.clients.removePipeline(client.id, id).then(e => {
            props.onUpdate();
        }).catch(res => {

        });
    };

    return [
            <SortableTableCRUD
                headers={[{
                    id: "name",
                    label: "Name"
                },
                {
                    id: "description",
                    label: "Description"
                },
                {
                    id: "timePeriodDefinition",
                    label: "Time Period"
                },
                {
                    id: "actions",
                    label: "Actions"
                },]}
                data={client.pipeline_list}
                row={(row, index) => {
                    return <TableRow>
                        <TableCell>{row.pipeline.name}</TableCell>
                        <TableCell></TableCell>
                        <TableCell>{row.schedule.name}</TableCell>
                        <TableCell>
                            <Button href={"/pipeline/" + row.pipeline_id}>View</Button>
                            <Button onClick={deletePipeline(row.pipeline_id)}>Delete</Button>
                        </TableCell>
                    </TableRow>
                }}
                title="Attached pipelines"
                dialogTitle="Attach pipeline"
                onDialogAdd={onDialogAdd}
                dialogFields={[
                    {
                        id: "id",
                        label: "Pipeline",
                        type: "select",
                        required: true,
                        data: availablePipelines.map(x => ({id: x.id, label: x.name}))
                    },
                    {
                        id: "schedule",
                        label: "Schedule",
                        type: "select",
                        required: true,
                        data: availableTimePeriods.map(x => ({id: x.id, label: x.name}))
                    }
                ]}
            />
    ];
});
