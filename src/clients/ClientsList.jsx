import React, { useState, useEffect }from "react";

import Typography from "@material-ui/core/Typography";
import SortableTableCRUD from "../common/SortableTableCRUD";

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';

import { withAPIAccess } from "../api/APIAccess";
import { useHistory } from "react-router-dom";


export default withAPIAccess(function ClientList(props) {
    const { api } = props;

    const history = useHistory();

    const [clients, setClients] = useState(null);

    useEffect(() => {
        api.clients.get().then(clients => {
            setClients(clients);
        });
    }, []);

    const onClick = client => e => {
        history.push("/client/" + client.id);
    };

    const deleteClient = mapping => e => {
        api.clients.delete(mapping.id).then(e => {
            setClients(clients.filter(x => x.id != mapping.id));
        });
    }

    const onDialogAdd = values => {
        api.clients.create(values.name, values.status).then(client => {
            setClients([...clients, client]);
        });
    }

    return <SortableTableCRUD
        headers={[{
            id: "name",
            label: "Name"
        },
        {
            id: "status",
            label: "Status"
        },
        {
            id: "actions",
            label: "Actions"
        }]}
        loading={clients === null}
        data={clients || []}
        paginate={true}
        paginationStartRows={5}
        paginationOptions={[5, 10, 20, 50]}
        row={(row, index) => {
            return <TableRow>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.status}</TableCell>
                <TableCell>
                    <Button color="primary" onClick={onClick(row)}>View</Button>
                    <Button color="primary" onClick={deleteClient(row)}>Delete</Button>
                </TableCell>
            </TableRow>
        }}
        fullWidth
        title="Clients"
        onDialogAdd={onDialogAdd}
        dialogTitle="Add client"
        dialogFields={[
            {
                id: "name",
                label: "Name",
                required: true
            },
            {
                id: "status",
                label: "Status",
                required: true
            }
        ]}/>
});
