import React, { useState, useEffect } from "react";

import FileCardManager from "../files/FileCardManager";

import AppToolbar from "../common/AppToolbar";

import { withAPIAccess } from "../api/APIAccess";

import PipelineTable from "./PipelineTable";
import TimePeriodsTable from "./TimePeriodsTable";

import FilesList from "../files/FilesList";

export default withAPIAccess(function Client(props) {
    const [client, setClient] = useState(null);

    const { api } = props;
    const { id } = props.match.params;

    useEffect(() => {
        api.clients.get(id).then(client => {
            setClient(client);
        })
    }, [id]);

    if (!client)
        return null;

    return [
        <AppToolbar title={client.name} />,
        <PipelineTable client={client} pipelines={client.pipeline_list} onUpdate={() => {
            api.clients.get(id).then(client => {
                setClient(client);
            })
        }}/>,
        <TimePeriodsTable client={client} />,
        <FilesList fullWidth={false} prefix={"clients/" + id}/>
    ]
});
