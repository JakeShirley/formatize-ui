export default [
    {
        name: "Home",
        description: "Data conversion tool",
        link: "/",
        adminLink: "/",
        updates: 0
    },
    {
        name: "Mappings",
        description: "View mappings",
        link: "/mappings",
        adminLink: "/mappings",
        updates: 0
    },
    {
        name: "Validators",
        description: "View validators",
        link: "/validators",
        adminLink: "/validators",
        updates: 0
    },
    {
        name: "Pipelines",
        description: "View pipelines",
        link: "/pipelines",
        adminLink: "/pipelines",
        updates: 0
    },
    {
        name: "Transforms",
        description: "View transforms",
        link: "/transforms",
        adminLink: "/transforms",
        updates: 0
    },
    {
        name: "Schedules",
        description: "View schedules",
        link: "/schedules",
        adminLink: "/schedules",
        updates: 0
    },
    {
        name: "Clients",
        description: "View clients",
        link: "/clients",
        adminLink: "/clients",
        updates: 0
    },
    {
        name: "Files",
        description: "View your files",
        link: "/files",
        adminLink: "/files",
        updates: 0
    },
    {
        name: "Jobs",
        description: "View jobs",
        link: "/jobs",
        adminLink: "/jobs",
        updates: 0
    },
    {
        name: "API Keys",
        description: "View API keys",
        link: "/api-keys",
        adminLink: "/api-keys",
        updates: 0
    },
    {
        name: "Logout",
        description: "",
        adminLink: "/logout",
        link: "/logout",
        updates: 0
    }
];
