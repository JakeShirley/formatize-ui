import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';


import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';

import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';

import { withAPIAccess } from "../api/APIAccess";

import withFileSelector from "../files/FileSelector";
import AppToolbar from "../common/AppToolbar";

import { readLocalFileHeaders } from "../files/Utils.js";

import DialogBoxGenerator from "../common/DialogBoxGenerator";

const useStyles = makeStyles(theme => ({
    paper: {
        width: "100%",
        height: "100%",
    }
}));

export default withAPIAccess(withFileSelector(function Validator(props) {
    const { api, fileSelector } = props;

    const { id } = props.match.params;

    const classes = useStyles();

    const [loadedChecked, setLoadedChecked] = React.useState([]);
    const [loadedHeaders, setLoadedHeaders] = React.useState([]);
    const [headers, setHeaders] = React.useState([]);
    const [checked, setChecked] = React.useState([]);

    const [addDialogOpen, setAddDialogOpen] = React.useState(false);

    const [validator, setValidator] = React.useState(null);

    useEffect(() => {
        api.validators.get(id).then(validator => {
            setValidator(validator);
            setHeaders(validator.columns || []);
        });
    }, [id]);

    const handleToggle = (checked, setChecked, value) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };

    const customList = (title, checked, setChecked, items) => {
        const selectAll = e => {
            setChecked(items);
        };
    
        const selectNone = e => {
            setChecked([]);
        };

        return <Paper className={classes.paper}>
            <Grid container direction="column" alignItems="center">
                <Typography variant="h5" component="h5">{title}</Typography>
                <Divider />
                <ButtonGroup>
                    { items.length > 0 && [<Button color="primary" onClick={selectAll}>Select all</Button>,
                    <Button color="primary" onClick={selectNone}>Select none</Button>]}
                </ButtonGroup>
            </Grid>
        <List dense component="div" role="list">
            {items.map(value => {
            const labelId = `transfer-list-item-${value}-label`;

            return (
                <ListItem key={value} role="listitem" button onClick={handleToggle(checked, setChecked, value)}>
                <ListItemIcon>
                    <Checkbox
                    checked={checked.indexOf(value) !== -1}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ 'aria-labelledby': labelId }}
                    />
                </ListItemIcon>
                <ListItemText id={labelId} primary={value} />
                </ListItem>
            );
            })}
            <ListItem />
        </List>
        </Paper>
    }

    fileSelector.onchange = e => {
        readLocalFileHeaders(e.target.files[0]).then(res => {
            setLoadedHeaders(res);
        });
    }

    const chooseFile = e => {
        fileSelector.click();
    }

    const add = e => {
        setAddDialogOpen(true);
    }

    const handleDialogClose = values => {
        if (values) {
            let newHeaders = [...headers, values.header];
            api.validators.update(id, {
                columns: newHeaders
            }).then(res => {
                setHeaders(newHeaders);
                setChecked([...checked, values.header]);
            });
        }
        setAddDialogOpen(false);
    }

    const moveColumnBack = e => {
        let newLoadedHeaders = [...loadedHeaders, ...checked];
        let newHeaders = headers.filter(x => !checked.includes(x));

        api.validators.update(id, {
            columns: newHeaders
        }).then(() => {
            setChecked([]);
            setHeaders(newHeaders);
            setLoadedHeaders(newLoadedHeaders);
        });
    };

    const moveColumnForward = e => {
        let newHeaders = [...headers, ...loadedChecked];
        let newLoadedHeaders = loadedHeaders.filter(x => !loadedChecked.includes(x));

        api.validators.update(id, {
            columns: newHeaders
        }).then(() => {
            setLoadedChecked([]);
            setLoadedHeaders(newLoadedHeaders);
            setHeaders(newHeaders);
        });
    };

    return [<AppToolbar title="Validator" headerControls={[
            <Button color="inherit" onClick={chooseFile}>Load headers</Button>,
            <Button color="inherit" onClick={add}>Add</Button>
        ]}/>,
    <Grid container justify="center">
        <Grid item xs={5}>
            { customList("Staging", loadedChecked, setLoadedChecked, loadedHeaders) }
        </Grid>
        <Grid item xs={1}>
            <Grid container direction="column" alignItems="center">
                <IconButton onClick={moveColumnBack}><ArrowBack /></IconButton>
                <IconButton onClick={moveColumnForward}><ArrowForward /></IconButton>
            </Grid>
        </Grid>
        <Grid item xs={5}>
            { customList("Headers", checked, setChecked, headers) }
        </Grid>
    </Grid>,
    <DialogBoxGenerator 
        title="Add header"
        open={addDialogOpen} 
        handleClose={handleDialogClose}
        fields={[{
            type: "text",
            id: "header",
            label: "Header name"
        }]} 
    />];
}));
