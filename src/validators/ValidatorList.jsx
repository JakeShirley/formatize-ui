import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';

import SortableTableCRUD from "../common/SortableTableCRUD";
import { withAPIAccess } from "../api/APIAccess";

export default withAPIAccess(function ValidatorList(props) {
    const { api } = props;
    const [validators, setValidators] = useState(null);

    useEffect(() => {
        api.validators.get().then(validators => {
            setValidators(validators);
        });
    }, []);

    const onDialogAdd = values => {
        api.validators.create(values.name, values.description).then(validator => {
            setValidators([...validators, validator]);
        });
    };

    const deleteValidator = row => e => {
        api.validators.delete(row.id).then(validator => {
            setValidators(validators.filter(x => x.id != row.id));
        });
    }

    return [
        <SortableTableCRUD
        headers={[
            {
                id: "name",
                label: "Name"
            },
            {
                id: "description",
                label: "Description"
            },
            {
                id: "actions",
                label: "Actions"
            }]
        }
        loading={validators === null}
        data={validators || []}
        paginate={true}
        paginationStartRows={5}
        paginationOptions={[5, 10, 20, 50]}
        row={(row, index) => {
            return <TableRow>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.description}</TableCell>
                <TableCell>
                    <Button color="primary" href={"/validator/" + row.id}>View</Button>
                    <Button color="primary" onClick={deleteValidator(row)}>Delete</Button>
                </TableCell>
            </TableRow>
        }}
        fullWidth
        title="Validators"
        onDialogAdd={onDialogAdd}
        dialogTitle="Add validator"
        dialogFields={[
            {
                id: "name",
                label: "Name",
                required: true
            },
            {
                id: "description",
                label: "Description",
                multiline: true,
                required: true
            }
        ]}/>];
});
