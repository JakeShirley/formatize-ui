import React from 'react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
    overrides: {
      // Style sheet name ⚛️
      
    },
  });

  function DefaultTheme(props) {
    return <ThemeProvider theme={theme}>
        {props.children}
    </ThemeProvider>
  }

  export default DefaultTheme;