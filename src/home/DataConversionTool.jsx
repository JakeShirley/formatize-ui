import React, { useState, useEffect } from "react";

import { withAPIAccess } from "../api/APIAccess";
import withUser from "../login/UserAccess";

import { useHistory } from "react-router-dom";

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

import AppToolbar from "../common/AppToolbar";

import JobGroupPoll from "../jobs/JobGroupPoll";

import Section from "../common/Section";
import FileCard from "../files/FileCard";

export default withUser(withAPIAccess(function DataConversionTool(props) {
    const { api, user } = props;

    const [selectedMappings, setSelectedMappings] = useState("");
    const [mappings, setMappings] = useState([]);
    const [jobs, setJobs] = useState([]);
    const [jobGroup, setJobGroup] = useState(null);
    const [jobGroupHref, setJobGroupHref] = useState(null);

    useEffect(() => {
        api.mappings.get().then(mappings => {
            setMappings(mappings);
        });
    }, []);

    if (!user?.profile)
        return null;

    const run = async e => {
        //if (!selectedMapping)
            //return alert("No mapping selected");

        let files = (await api.files.get(`admins/${user.profile.sub}/temp`)).filter(x => !x.name.includes("/combined/"));

        let jobGroup = await api.jobs.createGroup(`admins/${user.profile.sub}/temp/combined/output.csv`, files.length);

        let p = files.map((file, i) => api.jobs.create(file.name, selectedMappings[i] || null, jobGroup.id));

        setJobs(await Promise.all(p));
        setJobGroup(jobGroup);
        setJobGroupHref(await api.files.getFileLink(jobGroup.output_name));
    }

    return <Section title="Data conversion tool">
        <Box p={2}>
            <FileCard 
                type={
                    {
                        name: "Upload files",
                        description: "Please upload file(s) to be run through a mapping"
                    }
                }
                jobs={jobs}
                adminUpload
                adminEmail={user.profile.sub}
                isAdmin
                mappings={mappings}
                filter={file => !file.name.includes("/combined/")}
                onChange={(files, selectedMappings) => {
                    setSelectedMappings(selectedMappings);
                }}
            />
            
            <Box mt={2}>
                <Button color="primary" onClick={run}>Run</Button>
                <JobGroupPoll jobGroupId={jobGroup?.id} onPoll={async group => {
                    setJobGroup(group);
                }}>
                    <Button disabled={ jobGroup?.status !== "DONE"} color="primary" href={jobGroupHref}>Download combined output</Button>
                </JobGroupPoll>
            </Box>
        </Box>
    </Section>
}));