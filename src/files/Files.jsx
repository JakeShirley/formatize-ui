import React, { useState, useEffect } from "react";
import FilesList from "./FilesList";

import { withAPIAccess } from "../api/APIAccess";
import { useHistory } from "react-router-dom";

import withFileSelector from "./FileSelector";

import withUser from "../login/UserAccess";

import AppToolbar from "../common/AppToolbar";

import DataConversionTool from "../home/DataConversionTool";

export default withUser(withAPIAccess(function Files(props) {
    const { api, user } = props;

    const [selectedMapping, setSelectedMapping] = useState("");
    const [mappings, setMappings] = useState([]);
    const [jobs, setJobs] = useState([]);

    useEffect(() => {
        api.mappings.get().then(mappings => {
            setMappings(mappings);
        });
    }, []);

    if (!user?.profile)
        return null;

    const run = async e => {
        if (!selectedMapping)
            return alert("No mapping selected");

        let files = await api.files.get(`admins/${user.profile.sub}/temp`)

        let p = files.map(file => api.jobs.create(selectedMapping, file.name));

        setJobs(await Promise.all(p));
    }

    return [
    <AppToolbar title={"Files"} headerControls={[]}/>,
    <FilesList 
        title="Uploaded"
        fullWidth={false}
        prefix={`admins/${user.profile.sub}`}
        exclude={"generated/"}
    />, 
    <FilesList 
        title="Generated"
        fullWidth={false}
        prefix={`admins/${user.profile.sub}/generated`}
    />]
}));
