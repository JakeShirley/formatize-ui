import React, { useState, useEffect } from "react";

import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import FileCard from "./FileCard";

import FileAPI from "../api/FileAPI.js";

import { withAPIAccess } from "../api/APIAccess.jsx";

export default withAPIAccess(function FileCardManager(props) {
    const { api, timePeriodId, client, types, files, adminUpload, adminEmail, isAdmin } = props;
    const [shouldRefresh, setShouldRefresh ] = useState(false);

    return <Box m={2}><Grid container spacing={0}>{ types.map(x => {
        return <Grid item xs={12}>
            <FileCard
                /*onSelectFile={upload(x)}
                onDownload={download}
                onDelete={deleteObject}*/
                adminUpload={adminUpload}
                adminEmail={adminEmail}
                client={client}
                timePeriodId={timePeriodId}
                type={x}
                isAdmin={isAdmin}
            />
        </Grid>
    }) }</Grid></Box>
});
