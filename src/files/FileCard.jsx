import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import IconButton from "@material-ui/core/IconButton";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import DoneIcon from "@material-ui/icons/Done";
import Collapse from '@material-ui/core/Collapse';
import TextField from '@material-ui/core/TextField';

import Select from '@material-ui/core/Select';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

import green from '@material-ui/core/colors/green';
import orange from '@material-ui/core/colors/orange';
import red from '@material-ui/core/colors/red';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

import Box from '@material-ui/core/Box';

import CircularProgress from '@material-ui/core/CircularProgress';

import FileAPI from "../api/FileAPI";

import { withAPIAccess } from "../api/APIAccess";

import { withAuth } from '@okta/okta-react';

import withFileSelector from "./FileSelector";

import JobPoll from "../jobs/JobPoll";

import JobFinder from "../jobs/JobFinder";

const useStyles = makeStyles(theme => ({
    card: {
        minWidth: 275,
    },
    fullWidth: {
        padding: 0,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
    buttonStyle: {
        ...theme.typography.button,
        fontSize: theme.typography.pxToRem(13),
        float: "right"
    },
    approved: {
        color: green[500]
    },
    awaitingApproval: {
        color: orange[500],
    },
    rejected: {
        color: red[500]
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
}));

function UploadButton(props) {
    const classes = useStyles();

    if (props.file) {
        return <Button color="primary" size="small" disabled {...props}>Uploaded <DoneIcon className={classes.rightIcon} /></Button>
    } else {
        return <Button color="primary" size="small" {...props}>Upload</Button>
    }
}

const JOB_STATUSES = [
    {
        name: "NOT_STARTED",
        error: false
    },
    {
        name: "STARTED",
        error: false
    },
    {
        name: "DONE",
        error: false
    },
    {
        name: "IN_PROGRESS",
        error: false
    },
    {
        name: "NO_MAPPING_FOUND",
        error: true
    },
    {
        name: "VALIDATION_ERROR",
        error: true
    },
    {
        name: "ERROR",
        error: true
    }
]

export default withAPIAccess(withFileSelector(function FileCard(props) {
    const classes = useStyles();

    const { api, fileSelector, isAdmin, adminUpload, adminEmail, client, timePeriodId, type, fullWidth, filter, onChange, mappings = []} = props;

    fileSelector.onchange = e => {
        for (let file of e.target.files) {
            upload(file);
        }
    };

    const [files, setFiles] = useState([]);
    const [selectedMappings, setSelectedMappings] = useState([]);
    const [jobs, setJobs] = useState(props.jobs ?? []);

    useEffect(() => {
        setJobs(props.jobs ?? []);
    }, [props.jobs])

    useEffect(() => {
        if (!props.files) {
            //setLoading(true);
            let filter = adminUpload ? `admins/${adminEmail}/temp` : `clients/${client.id}/uploaded/${timePeriodId}/${type.id}`;
            api.files.get(filter).then(async files => {
                if (props.filter)
                    files = files.filter(props.filter);
      
                let newSelectedMappings = files.map(x => "");

                setFiles(files);
                setSelectedMappings(newSelectedMappings);
                onChange && onChange(files, newSelectedMappings);
            });
        }
    }, []);

    const dragListener = e => {
        e.preventDefault()
        e.stopPropagation()
    }

    const dropListener = e => {
        e.preventDefault()
        e.stopPropagation()

        for (let file of e.dataTransfer.files) {
            upload(file);
        }
    }

    const upload = file => {
        const key = adminUpload ? 
            `admins/${adminEmail}/temp/${file.name}` 
            : `clients/${client.id}/uploaded/${timePeriodId}/${type.id}/${file.name}`;

        setFiles([...files.filter(x => x.name !== key), {name: file.name, loading: true} ]);
        setJobs(jobs.filter(x => x.filename !== key));
        return api.files.upload(key, file).then(res => {
            api.files.aboutFile(key).then(async file => {
                let q = await api.jobs.query(key);

                file.tempJobId = q.pop()?.id;

                let newFiles = [...files.filter(x => x.name !== key), file];
                let newSelectedMappings = [...selectedMappings, ""];

                setFiles(newFiles);
                setSelectedMappings(newSelectedMappings);

                onChange && onChange(newFiles, newSelectedMappings);
            });
        });
    }
    
    const download = file => {
        api.files.getFile(file).then(res => {
            var blob = new Blob([res], { type: "text/plain" });

            var objectUrl = URL.createObjectURL(blob);

            const link = document.createElement('a');
            link.style.display = 'none';
            document.body.appendChild(link);

            link.href = objectUrl;
            link.href = URL.createObjectURL(blob);
            link.download = file;
            link.click();
        });
    }

    const deleteObject = (file, i) => e => {
        api.files.delete(file.name).then(res => {
            //setShouldRefresh(!shouldRefresh);
            let newFiles = files.filter((x, i) => x.name !== file.name);
            let newSelectedMappings = selectedMappings.filter((x, j) => i !== j);
            setFiles(newFiles);
            setJobs(jobs.filter(x => x.filename !== file.name));
            setSelectedMappings(newSelectedMappings);

            onChange && onChange(newFiles, newSelectedMappings);
        });
    }

    return <JobFinder
    paused={adminUpload}
    files={files}
    onPoll={newJobs => {
        setJobs(newJobs);
    }} ><JobPoll paused={false} jobs={jobs} onPoll={jobs => {
        setJobs(jobs);
        //console.log("OKOK", jobs);
    }} interval={1000}>
        <Card className={classes.card} elevation={4}>
            <CardContent>
                <Typography align="left" variant="h5" component="h2" gutterBottom>
                    {type.name}
                </Typography>
                <Typography align="left" color="textSecondary">
                    {type.description}
                </Typography>  
            </CardContent>
            <Box p={2} 
                mt={2} 
                mb={2} 
                height={150} 
                style={{borderColor: "grey", borderStyle: "dashed", background: "#eaeaea", color:"grey"}} 
                onDragOver={e => {}} 
                onDragLeave={dragListener} 
                onDragOver={dragListener} 
                onDrop={dropListener}>
                <Box display="flex" height="100%" alignItems="center" justifyContent="center">
                    <Box display="flex" flexDirection="row" alignItems="center" justifyContent="center">
                        Drag a file here to upload it<Box component="span" m="auto" ml={1}><CloudUploadIcon /></Box>
                    </Box>
                </Box>
            </Box>
            <CardContent>
                { files.map((file, i) => {
                    const job = jobs.find(x => x.filename === file.name);
                    const loading = file.loading || (!adminUpload && typeof job?.status === "undefined") || (job && job.status !== "DONE");
                    const notDeletable = file.loading || (!adminUpload && typeof job?.status === "undefined") || (job && (job.status === "STARTED" || job.status === "NOT_STARTED"));

                    let error = !jobs[i]?.status || JOB_STATUSES.find(x => x.name === jobs[i]?.status).error;

                    const setMapping = (val) => {
                        let newSelectedMappings = selectedMappings.map((x, j) => i === j ? val : x);
                        setSelectedMappings(newSelectedMappings);
                        onChange && onChange(files, newSelectedMappings);
                    }

                    return <Box display="flex" mt={1} mb={1}>
                        <Box m="auto">
                            <Typography variant="subtitle1" component="span">{ file.name.split("/").pop() }</Typography>
                        </Box>
                        <div style={{ flexGrow: "1" }}></div>
                        { error && <Box m="auto" ml={2}>
                            <Typography color="error" variant="subtitle2" component="span">{ jobs[i]?.status }</Typography>
                        </Box>  }
                        {mappings.length > 0 && <Box m="auto" ml={1} width="30%">
                            <TextField
                                select
                                variant="outlined"
                                label="Auto-detect mapping"
                                fullWidth
                                onChange={e => {
                                    setMapping(e.target.value);
                                }}
                                value={selectedMappings[i]}
                            >
                                <MenuItem onClick={e => setMapping(null)}>Auto-detect</MenuItem>

                                {mappings.map(mapping => {
                                    return <MenuItem value={mapping.id}>{mapping.id}</MenuItem>
                                })}
                            </TextField>
                        </Box>}
                        { isAdmin && <Box m="auto" ml={1}>
                            <Button 
                                disabled={!job || loading} variant="outlined"
                                color="primary" 
                                onClick={e => download(job?.output_filename)}>
                                    Download output {(loading && notDeletable) && <CircularProgress color="inherit" size={20} style={{marginLeft: 5}}/>}
                            </Button>
                        </Box> }
                        <Box m="auto" ml={1}>
                            <Button disabled={notDeletable} variant="outlined" color="primary" onClick={deleteObject(file, i)}>Delete</Button>
                        </Box>
                    </Box>
                })}
            </CardContent>
            <CardActions>
                <UploadButton onClick={e => fileSelector.click()}/>
            </CardActions>
        </Card>
    </JobPoll>
    </JobFinder>
}));
