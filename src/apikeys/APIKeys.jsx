import React, { useState, useEffect }from "react";

import Typography from "@material-ui/core/Typography";
import SortableTableWithHeader from "../common/SortableTableWithHeader";

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';

import { withAPIAccess } from "../api/APIAccess";
import { useHistory } from "react-router-dom";


export default withAPIAccess(function APIKeys(props) {
    const { api } = props;

    const history = useHistory();

    const [apiKeys, setAPIKeys] = useState(null);

    useEffect(() => {
        api.keys.get().then(keys => {
            setAPIKeys(keys);
        });
    }, []);

    const deleteKey = key => e => {
        api.keys.delete(key.id).then(e => {
            setAPIKeys(apiKeys.filter(x => x.id != key.id));
        });
    }

    const addKey = e => {
        api.keys.create().then(key => {
            console.log(key);
            setAPIKeys([...apiKeys, key]);
        });
    }

    return <SortableTableWithHeader
        headers={[{
            id: "key",
            label: "API Key"
        }]}
        loading={apiKeys === null}
        data={apiKeys || []}
        paginate={true}
        paginationStartRows={5}
        paginationOptions={[5, 10, 20, 50]}
        row={(row, index) => {
            return <TableRow>
                <TableCell>{row.api_key}</TableCell>
                <TableCell>
                    <Button color="primary" onClick={deleteKey(row)}>Delete</Button>
                </TableCell>
            </TableRow>
        }}
        fullWidth
        title="API Keys"
        headerControls={apiKeys ? [
            <Button color="inherit" onClick={addKey}>Add</Button>
        ] : []}/>
});
