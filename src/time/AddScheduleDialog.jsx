import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';

import SortableTableWithHeader from "../common/SortableTableWithHeader";
import { withAPIAccess } from "../api/APIAccess";

import Button from '@material-ui/core/Button';

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import DialogBox from "../common/DialogBox";

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import Radio from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox';

import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import AppbarTextField from "../common/AppbarTextField";

import {
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

const useStyles = makeStyles(theme => ({
    formControl: {
        //marginTop: theme.spacing(2)
    }
}));

const PERIOD_OPTIONS = [
  "day",
  "week",
  "month",
  "year"
];

export default withAPIAccess(function AddScheduleDialog(props) {
    const { api, onDialogAdd } = props;
    const [values, setValues] = useState({
        start: new Date(),
        duration: 28,
        repeatEvery: 1,
        repeatPeriod: "day"
    });

    const classes = useStyles();

    const onChange = (e, n) => {
        if (e.target) {
            const { name, value, checked, type } = e.target;
            setValues({...values, [name]: type == "checkbox" ? checked : value});
        } else {
            setValues({...values, [n]: e});
        }
    }

    return <DialogBox title="Create time period" {...props} handleClose={onDialogAdd(null)} actions={[
            <Button onClick={onDialogAdd(values)}>Add</Button>
        ]}>
            <TextField variant="outlined" margin="normal" fullWidth name="name" value={values.name} onChange={onChange} label="Name of the schedule"/>
            <TextField variant="outlined" margin="normal" multiline rows="4" fullWidth name="description" value={values.description} onChange={onChange} label="Description of the schedule"/>
            <KeyboardDatePicker
              disableToolbar
              margin="normal"
              format="dd/MM/yyyy"
              margin="normal"
              id="start"
              name="start"
              label="Start date"
              value={values.start}
              onChange={e => onChange(e, "start")}
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
              fullWidth
              required={true}
            />
          <Grid container spacing={1} alignItems="center">
              <Grid item xs={8}>
                Duration in days
              </Grid>
              <Grid item xs={4}>
                <TextField fullWidth name="duration" value={values.duration} onChange={onChange} placeholder="1" type="number" />
              </Grid>
          </Grid>
          <FormControlLabel value="recurring" name="recurring" control={<Checkbox value={values.recurring} onChange={onChange}/>} label="Recurring?" />
          { values.recurring &&
          [<Grid container spacing={1} alignItems="flex-end">
              <Grid item xs={4}>
                Repeat every
              </Grid>
              <Grid item xs={4}>
                <TextField fullWidth name="repeatEvery" value={values.repeatEvery} onChange={onChange} placeholder="1" type="number" />
              </Grid>
              <Grid item xs={4}>
                <Select fullWidth name="repeatPeriod" value={values.repeatPeriod} onChange={onChange}>
                    { PERIOD_OPTIONS.map(x => {
                        return <MenuItem key={x} value={x}>{x + (values.repeatEvery > 1 ? "s" : "")}</MenuItem>
                    })}
                </Select>
              </Grid>
            </Grid>,
            <FormControlLabel value="backdated" name="backdated" control={<Checkbox value={values.backdated} onChange={onChange}/>} label="Include backdated?" />,
            <FormControl component="fieldset" className={classes.formControl} fullWidth>
               <Grid container alignItems="center" spacing={1}>
                   <Grid item xs={4}>
                       <FormControlLabel value="until" control={<Checkbox name="untilState" value={values.untilState} onChange={onChange}/>} label="Until" />
                   </Grid>
                   <Grid item xs={8}>
                        { values.untilState && <KeyboardDatePicker
                         disableToolbar
                         margin="normal"
                         format="dd/MM/yyyy"
                         margin="normal"
                         id="end"
                         name="end"
                         value={values.end}
                         onChange={e => onChange(e, "end")}
                         KeyboardButtonProps={{
                           'aria-label': 'change date',
                         }}
                         fullWidth
                         required={true}
                       /> }
                   </Grid>
               </Grid>
           </FormControl>] }
        </DialogBox>
});
