import React, { useState, useEffect } from "react";

import SortableTableWithHeader from "../common/SortableTableWithHeader";

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';

import AddScheduleDialog from "./AddScheduleDialog";

import { withAPIAccess } from "../api/APIAccess";

export default withAPIAccess(function SchedulesList(props) {
    const [schedules, setTimePeriods] = useState(props.schedules);
    const [availableTimePeriods, setAvailableTimePeriods] = useState([]);

    const [open, setOpen] = useState(false);

    const { api, client } = props;

    useEffect(() => {
        api.schedules.get().then(schedules => {
            setTimePeriods(schedules);
        });
    }, []);

    const deleteTimePeriod = id => e => {
        api.schedules.delete(id).then(e => {
            setTimePeriods(schedules.filter(x => x.id != id));
        }).catch(res => {

        });
    };

    const onDialogAdd = values => e => {
        setOpen(false);

        if (!values)
            return;

        api.schedules.create(values.name, values.start, values.end, values.duration, values.repeatEvery, values.repeatPeriod, values.recurring).then(timePeriod => {
            setTimePeriods([...schedules, timePeriod]);
        }).catch(res => {

        });
    };

    return [<AddScheduleDialog open={open} onDialogAdd={onDialogAdd}/>,
            <SortableTableWithHeader
                headers={[{
                    id: "name",
                    label: "Name"
                },
                {
                    id: "description",
                    label: "Description"
                },
                {
                    id: "start",
                    label: "Start"
                },
                {
                    id: "end",
                    label: "End"
                },
                {
                    id: "duration",
                    label: "Duration"
                },
                {
                    id: "recurring",
                    label: "Recurring"
                },
                {
                    id: "repeatEvery",
                    label: "Repeat every"
                },
                {
                    id: "repeatPeriod",
                    label: "Repeat period"
                },
                {
                    id: "actions",
                    label: "Actions"
                },]}
                loading={!schedules}
                data={schedules || []}
                row={(row, index) => {
                    return <TableRow>
                        <TableCell>{row.name}</TableCell>
                        <TableCell>{row.description}</TableCell>
                        <TableCell>{row.start}</TableCell>
                        <TableCell>{row.end}</TableCell>
                        <TableCell>{row.duration}</TableCell>
                        <TableCell>{row.recurring ? "Yes" : "No"}</TableCell>
                        <TableCell>{row.repeat_every}</TableCell>
                        <TableCell>{row.repeat_period + (row.repeat_every > 1 ? "s" : "")}</TableCell>
                        <TableCell>
                            <Button onClick={deleteTimePeriod(row.id)}>Delete</Button>
                        </TableCell>
                    </TableRow>
                }}
                fullWidth
                paginate={true}
                paginationStartRows={5}
                paginationOptions={[5, 10, 20, 50]}
                title="Schedules"
                headerControls={schedules ? [
                    <Button color="inherit" onClick={e => setOpen(!open)}>Create</Button>
                ] : []}
            />
    ];
});
