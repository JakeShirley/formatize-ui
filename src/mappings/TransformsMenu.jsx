import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';

import green from '@material-ui/core/colors/green';
import yellow from '@material-ui/core/colors/yellow';
import red from '@material-ui/core/colors/red';
import blue from '@material-ui/core/colors/blue';

import SortableTableWithHeader from "../common/SortableTableWithHeader";

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';

import Box from '@material-ui/core/Box';
import TableCell from '@material-ui/core/TableCell';

import MenuList from '@material-ui/core/MenuList';
import Paper from '@material-ui/core/Paper';

import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';

import { withAPIAccess } from "../api/APIAccess";
import Typography from '@material-ui/core/Typography';

import DialogBox from "../common/DialogBox";

import Popper from '@material-ui/core/Popper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import TextField from '@material-ui/core/TextField';

import TransformList from "./TransformList";

const useStyles = makeStyles(theme => ({
    menuItem: {
        color: '#1a1a1a',
        fontFamily: "Nunito Sans",
        fontSize: 14
    }
}));

const TRANSFORM_LABEL_MAP = {
    "rename-column": (name, values) => values?.dest?.value ? name + " to '" + values?.dest?.value + "'" : name
}

export default withAPIAccess(function TransformsMenu(props) {
    const { api, handleClose, selectedColumn, removeTransform, anchorEl, availableTransforms, transforms } = props;

    const [loadingRecommendations, setLoadingRecommendations] = useState(false);
    const [recommendations, setRecommendations] = useState([]);

    const classes = useStyles();

    useEffect(() => {
        if (selectedColumn?.origName) {
            setLoadingRecommendations(true);
            api.transforms.getRecommendations(selectedColumn.origName).then(recommendations => {
                setRecommendations(recommendations);
                setLoadingRecommendations(false);
                //console.log(selectedColumn, recommendations);
            });
        }
    }, [selectedColumn?.origName]);

    const clickAway = event => {
        if (anchorEl && anchorEl.contains(event.target)) {
            return;
        }

        handleClose(null)();
    }

    if (!anchorEl)
        return null;

    const recommendedTransforms = list => {
        return list.map( x => {
            let t = x.transform;
            if (!Object.keys(t.params).find(y => t.params[y].src))
                return null;

            let labelMapping = TRANSFORM_LABEL_MAP[t.name];

            return <TransformMenuItem key={t.name} onClick={handleClose(t, x)}>{labelMapping ? labelMapping(t.label, x.params) : t.label}</TransformMenuItem>
        });
    }

    const selectableTransforms = list => {
        return list.map( x => {
            if (!x || !Object.keys(x.params).find(y => x.params[y].src))
                return null;

            return <TransformMenuItem key={x.name} onClick={handleClose(x)}>{x.label}</TransformMenuItem>
        });
    }

    const TransformMenuItem = props => {
        return <MenuItem classes={{
            root: classes.menuItem
        }} {...props} />
    }

    const currentTransformsList = list => {
        return list.map( (transform, i) => {
            const trans = availableTransforms.find(y => y.name == transform.name);

            const show = Object.keys(trans.params).find(x => {
                let param = trans.params[x];
                if (param.type === "column" && param.src) {
                    if (transform.params[x].value == selectedColumn?.id) {
                        return true;
                    }
                }
            });
            if (show) {
                let labelMapping = TRANSFORM_LABEL_MAP[transform.name];

            //return <TransformMenuItem key={t.name} onClick={handleClose(t, x)}>{labelMapping ? labelMapping(t.label, x.params) : t.label}</TransformMenuItem>
                return <TransformMenuItem onClick={removeTransform(transform)}><DeleteIcon />{" "}{labelMapping ? labelMapping(trans.label, transform.params) : trans.label} (Index: {i})</TransformMenuItem>
            } else {
                return null;
            }
        });
    }

    return <Popper
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose(null)}
        style={{ zIndex: 100 }}
        >
        <Paper>
            <ClickAwayListener onClickAway={clickAway}>
                <Box>
                    <Box p={2}>
                        <TextField fullWidth variant="outlined" label="Search..." />
                    </Box>
                    <Divider />
                    <Box m={2}>
                        <Typography variant="subtitle2" component="h3">Recommended transforms</Typography>
                    </Box>
                    <Divider />
                    <MenuList>
                        { !loadingRecommendations && recommendations.length > 0 ? recommendedTransforms(recommendations) : !loadingRecommendations ? <TransformMenuItem>None to display...</TransformMenuItem> : null}
                        
                        { loadingRecommendations && <TransformMenuItem>Loading...</TransformMenuItem>}
                    </MenuList>
                    
                    <Divider />
                    <Box m={2}>
                        <Typography variant="subtitle2" component="h3">All transforms</Typography>
                    </Box>
                    <Divider />
                    <MenuList>
                        { selectableTransforms(availableTransforms) }
                        <Divider />
                        { currentTransformsList(transforms) }
                    </MenuList>
                </Box>
            </ClickAwayListener>
        </Paper>
    </Popper>
});
