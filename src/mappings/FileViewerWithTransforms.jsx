import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';

import green from '@material-ui/core/colors/green';
import yellow from '@material-ui/core/colors/yellow';
import red from '@material-ui/core/colors/red';
import blue from '@material-ui/core/colors/blue';

import SortableTableWithHeader from "../common/SortableTableWithHeader";

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';

import { withAPIAccess } from "../api/APIAccess";
import AddTransformDialog from "./AddTransformDialog";

import DialogBox from "../common/DialogBox";

import TransformList from "./TransformList";

import TransformsMenu from "./TransformsMenu";

const retry = require('promise-retry');

const useStyles = makeStyles(theme => ({
    greenColumn: {
        background: green[100]
    },
    yellowColumn: {
        background: yellow[100]
    },
    deleteTransform: {
        background: red[400]
    },
    blueColumn: {
        background: blue[100]
    }
}));

export default withAPIAccess(function FileViewerWithTransforms(props) {
    function createHeader(id, name, className) {
        return { id,
            numeric: false,
            name,
            origName: name,
            className,
            outerRender: (row) => {
            return <TableCell
                key={row.id}
                align="left"
                padding={row.disablePadding ? 'none' : 'default'}
                className={row.className}>
                <strong>{row.name} {!row.toBeRemoved && <IconButton onClick={e => {
                    setAnchorEl(e.target);
                    setSelectedColumn(row);
                }} size="small"><AddIcon /></IconButton> }</strong>
            </TableCell>
        }};
    }

    const classes = useStyles();

    const { api, mapping, file, stage } = props;

    const [rawHeaders, setRawHeaders] = React.useState([]);
    const [headers, setHeaders] = React.useState([]);
    const [data, setData] = React.useState([]);
    const [message, setMessage] = React.useState("");
    const [loading, setLoading] = useState(true);

    const [addTransformOpen, setAddTransformOpen] = useState(false);
    const [selectedTransform, setSelectedTransform] = useState(null);

    const [anchorEl, setAnchorEl] = React.useState(null);
    const [selectedColumn, setSelectedColumn] = React.useState(null);
    const [transforms, setTransforms] = React.useState([]);

    const [availableTransforms, setAvailableTransforms] = React.useState([]);

    const [viewTransforms, setViewTransforms] = React.useState(false);

    const [editingTransformInstance, setEditingTransformInstance] = React.useState(null);
    const [editingTransform, setEditingTransform] = React.useState(null);
    const [transformValues, setTransformValues] = React.useState({});

    useEffect(() => {
        Promise.all([
            api.mappings.getTransforms(),
            api.mappings.getAssignedTransforms(stage.id)
        ]).then(([transforms, assigned]) => {
            setAvailableTransforms(transforms.sort((a, b) => {
                return a.name.localeCompare(b.name);
            }));
            setTransforms(assigned);
        });
    }, []);

    function load() {
        retry(retry => api.files.preview(file).then(data => {
            setMessage("");
            setData(data.slice(1, data.length));

            let headers = [];
            if (data.length > 0) {
                headers = data[0];
            }
            setRawHeaders(headers);
            setLoading(false);
        }).catch(retry), { minTimeout: 5000 }).catch(res => {
            if (res.status == 404) {
                setLoading(false);
                setMessage("There's no output for this stage yet. Please run the mapping first.");
            }
        })
    }

    useEffect(() => {
        //setLoading(true);
        load();
    }, []);

    useEffect(() => {
        if (!props.jobStage)
            return;

        if (props.jobStage.status != "DONE") {
            setLoading(true);
        } else {
            load();
        }
    }, [props.jobStage?.status]);

    useEffect(() => {
        if (!loading) {
            props.onAddTransform();
        }
    }, [transforms.length]);

    useEffect(() => {

        let keepTransforms = transforms.filter(x => x.name == "keep-column");

        let addedColumns = transforms.filter(x => x.name == "add-column");
        let renameColumns = transforms.filter(x => x.name == "rename-column");

        let otherTransforms = transforms.filter(x => x.name != "add-column" && x.name != "rename-column" && x.name != "remove-column" && x.name != "keep-column");

        let headers = rawHeaders.map((x, i) => createHeader(i, x));

        headers = headers.map(x => {
            let found = keepTransforms.find(transform => {
                return transform.params.column.value == x.id;
            });

            return found ? { ...x, 
                name: x.name + " (" + (found.order + 1) + ")", 
                keep: found, 
                keepIndex: found.params.column.value, 
                className: found ? classes.greenColumn : classes.blueColumn
            } : x;
        });

        headers = [ ...headers, ...addedColumns.map(x => {
            return createHeader(x.params.column, x.params.column, classes.greenColumn);
        })];

        headers = headers.map(x => {
            const newName = renameColumns.find(transform => {
                return transform.params.src === x.id;
            });

            if (newName) {
                return { ...x, name: x.name + " -> " + newName.params.dest, origName: x.origName, className: classes.yellowColumn};
            } else {
                return x;
            }
        });

        headers = headers.map(x => {
            let found = otherTransforms.map(transform => {
                const trans = availableTransforms.find(y => y.name == transform.name);

                const show = Object.keys(trans.params).find(paramName => {
                    let param = trans.params[paramName];
                    if (param.type == "column" && param.src) {
                        if (transform.params[paramName] == x.id) {
                            return true;
                        }
                    }
                });

                return show ? transform : null;
            }).filter(x => x);

            return found.length ? { ...x, className: classes.blueColumn} : x;
        });

        setHeaders(headers);
    }, [rawHeaders, transforms]);

    const click = e => {
        setSelectedTransform(availableTransforms.find(x => x.name == "add-column"));
        setAddTransformOpen(true);
    };

    const addTransform = values => {
        if (!values) {
            setAddTransformOpen(false);
            setSelectedTransform(null);
            return;
        }

        api.mappings.assignTransform(stage.id, selectedTransform.name, transforms.length, values).then(instance => {
            setAddTransformOpen(false);
            setSelectedTransform(null);
            setSelectedColumn(null);

            setTransforms([...transforms, instance]);
        });
    }

    const moveTransform = up => transform => e => {
        if (up && transform.order === 1)
            return;

        if (!up && transform.order === transforms.length)
            return;

        let newId = up ? transform.order - 1 : transform.order + 1;

        let otherTransform = transforms.find(x => x.order == newId);

        Promise.all([
            api.mappings.setTransformOrder(transform.id, newId),
            api.mappings.setTransformOrder(otherTransform.id, transform.order)
        ]).then(([didUpdate]) => {
            setTransforms(transforms.map(x => {
                if (x.id == transform.id) {
                    return {...x, order: newId};
                } else if (x.id == otherTransform.id) {
                    return {...x, order: transform.order};
                }
                return {...x};
            }).sort((a, b) => a.order - b.order));
        });
    };

    const removeTransform = t => e => {
        api.mappings.unassignTransformInstance(stage.id, t.id).then(instance => {
            setTransforms(transforms.filter(x => x.id !== t.id));
        });
    };

    const editTransform = (t, instance) => e => {
        setEditingTransformInstance(instance);
        setEditingTransform(t);
    };

    const handleClose = (transform, instance) => e => {
        if (transform) {
            setSelectedTransform(transform);
            if (instance)
                setTransformValues(instance.params);

            setAddTransformOpen(true);
        }

        setAnchorEl(null);
        //setSelectedColumn(null);
    };

    return <>
        <TransformsMenu 
            handleClose={handleClose}
            removeTransform={removeTransform}
            transforms={transforms}
            availableTransforms={availableTransforms}
            anchorEl={anchorEl}
            selectedColumn={selectedColumn} 
        />

        <AddTransformDialog 
            selectedColumn={selectedColumn} 
            headers={headers} 
            transform={selectedTransform} 
            open={addTransformOpen}
            values={transformValues}
            handleClose={addTransform}
        />

        <DialogBox title="Transforms" open={viewTransforms} handleClose={e => setViewTransforms(false)}>
            <TransformList 
                transforms={transforms} 
                availableTransforms={availableTransforms} 
                deleteTransform={removeTransform} 
                moveUp={moveTransform(true)}
                moveDown={moveTransform(false)}
                edit={editTransform}
            />
            <AddTransformDialog
                action="Update"
                headers={headers} 
                open={Boolean(editingTransform)} 
                values={editingTransformInstance?.params}
                transform={editingTransform} 
                handleClose={values => {
                    if (values) {
                        const { id } = editingTransformInstance;
                        return api.mappings.updateTransform(id, {
                            params: values
                        }).then(e => {
                            setTransforms(transforms.map(x => {
                                if (x.id == id) {
                                    return {...x, params: values};
                                }
                                return {...x};
                            }));

                            setEditingTransform(null);
                        });
                    }
                    setEditingTransform(null);
                }} 
            />
        </DialogBox>

        <SortableTableWithHeader
            loading={loading}
            headers={headers}
            data={data}
            paginate={true}
            minRows={5}
            paginationStartRows={5}
            paginationOptions={[5, 10, 20]}
            row={(row, index) => {
                let output = {...row};
                return <TableRow key={index}>{ headers.map((x, i) => (<TableCell key={i} className={x.className}>{output[x.id]}</TableCell>)) }</TableRow>
            }}
            title={props.title}
            headerControls={[
                <Button key="view-transforms" onClick={e => setViewTransforms(true)} color="inherit">Transforms</Button>,
                <Button key="delete-self" onClick={props.onDelete(stage)} color="inherit">Delete</Button>,
                <IconButton key="Add" onClick={click} color="inherit"><AddIcon /></IconButton>
            ]} 
            text={message}
        />
    </>
});
