import React, { useState, useEffect } from "react";

import SortableTable from "../common/SortableTable";
import SortableTableWithHeader from "../common/SortableTableWithHeader";
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import { withAPIAccess } from "../api/APIAccess";

const retry = require('promise-retry');

export default withAPIAccess(function FileViewer(props) {
    const { filename, api, loading, hidden } = props;

    const [headers, setHeaders] = useState([]);
    const [internalLoading, setInternalLoading] = useState(false);
    const [data, setData] = useState([]);

    useEffect(() => {
        if (!filename)
            return;
        setInternalLoading(true);
        retry(retry => api.files.preview(filename).then(data => {
            setData(data.slice(1, data.length));
            if (data.length > 0) {
                setHeaders(data[0].map((x, i) => ({id: i, label: x})));
            }
            setInternalLoading(false);
        }).catch(retry), { minTimeout: 5000 });
    }, [filename]);

    if (hidden)
        return null;

    return <SortableTable
        loading={internalLoading || loading}
        headers={headers}
        data={data}
        paginate={true}
        minRows={5}
        paginationStartRows={5}
        paginationOptions={[5, 10, 20]}
        row={(row, index) => {
            return <TableRow key={index}>{ row.map((x, i) => (<TableCell key={i}>{x}</TableCell>)) }</TableRow>
        }} />
});
