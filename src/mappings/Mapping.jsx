import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';

import Typography from "@material-ui/core/Typography";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';

import Divider from '@material-ui/core/Divider';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

import FileViewer from "./FileViewer";

import { withAPIAccess } from "../api/APIAccess";

import AppToolbar from "../common/AppToolbar";

import JobPoll from "../jobs/JobPoll";
import FileViewerWithTransforms from "./FileViewerWithTransforms";

import DialogBoxGenerator from "../common/DialogBoxGenerator";

import withUser from "../login/UserAccess";

import StageList from "./StageList";
import TransformList from "./TransformList";


const csv = require("neat-csv");
const queryString = require('query-string');

const useStyles = makeStyles(theme => ({
    darker: {
        background: "#efefef"
    },
    output: {
        background: "#f3f3f3"
    },
}));

export default withUser(withAPIAccess(function DataViewer(props) {
    const { api, user } = props;

    const classes = useStyles();

    const { id } = props.match.params;
    const { file } = queryString.parse(props.location.search);

    const [mapping, setMapping] = useState(null);
    const [validators, setValidators] = useState([]);

    const [headers, setHeaders] = useState([]);
    const [data, setData] = useState([]);

    const [jobs, setJobs] = useState([]);

    const [update, setUpdate] = useState(0);
    const [openDialog, setOpenDialog] = useState(false);
    const [value, setValue] = useState(0);

    const [downloadLink, setDownloadLink] = useState("");

    useEffect(() => {
        let promises = [ api.mappings.get(id),
                         api.validators.get() ];
        Promise.all(promises).then(([mapping, validators]) => {
            setMapping(mapping);
            setValidators(validators);
        })
    }, []);

    useEffect(() => {
        setUpdate(update + 1);
    }, [jobs.filter(x => x.status == "DONE").length]);

    useEffect(() => {
        //run();
    }, [mapping?.validator]);

    useEffect(() => {
        if (!mapping)
            return;

        //run();
    }, [mapping]);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const run = async e => {
        let jobs = [];
        await api.jobs.create(file, id).then(j => {
            jobs = [...jobs, j];
        });
        setJobs(jobs);
    };

    const addStage = e => {
        setOpenDialog(true);
    }

    const deleteStage = stage => e => {
        api.mappings.removeStage(stage.id).then(s => {
            setMapping({...mapping, stages: mapping.stages.filter(x => x.id != stage.id)});
        });
    }

    const handleClose = values => {
        setOpenDialog(false);
        if (values) {
            api.mappings.addStage(mapping.id, values.name, values.description).then(stage => {
                setMapping({...mapping, stages: [...mapping.stages, stage]});
            });
        }
    }

    if (!mapping || !user?.token)
        return null;

    const onSelectChange = e => {
        console.log(e.target);

        if (e.target.value !== "") {
            setMapping({...mapping, validator: validators.find(x => x.id === e.target.value)});

            api.mappings.update(mapping.id, {
                validator: e.target.value
            });
        } else {
            setMapping({...mapping, validator: null});

            api.mappings.update(mapping.id, {
                validator: null
            });
        }
    };

    let jobStatus = "";
    if (jobs?.length > 0 && jobs.filter(x => x.status != "DONE").length == 0) {
        jobStatus = "Done";
    } else if (jobs.filter(x => x.status == "VALIDATION_ERROR").length > 0) {
        jobStatus = "Validation error. See log for more details.";
    } else {
        jobStatus = jobs.length == 0 ? "NOT STARTED" : "PROCESSING...";
    }

    return <>
    <DialogBoxGenerator title="Add stage" fields={[
        {
            id: "name",
            label: "Name",
            required: true
        },
        {
            id: "description",
            label: "Description",
            required: true
        }
    ]} open={openDialog} handleClose={handleClose}/>
    <AppToolbar title={"Mapping '" + mapping.id + "'"} headerControls={[
        <Button color="inherit" onClick={addStage}>Add Stage</Button>
    ]}/>
    <Box m={2}>
        <TextField value={mapping.validator?.id || ""} variant="outlined" select fullWidth label="Select a validator..." onChange={onSelectChange}>
            { validators.map(x => {
                return <MenuItem value={x.id}>{x.name}</MenuItem>
            })}
            <Divider />
            <MenuItem value="">None</MenuItem>
        </TextField>
    </Box>
    <Grid container spacing={0}>
        {mapping.stages.map((stage, i) => {
            let jobStage = i === 0 ? null : jobs[0]?.stages?.find(x => x.mapping_stage_id === mapping.stages[i - 1].id);
            let name = file.split("/").pop();
            return <Grid item sm={12}>
                <FileViewerWithTransforms
                    stage={stage}
                    title={stage.name}
                    onDelete={deleteStage}
                    jobStage={jobStage}
                    file={i == 0 ? file : "admins/" + user.profile.email + "/generated/" + mapping.id + "/" + i + "/" + name + ".csv"}
                    onAddTransform={transform => {
                        //run();
                    }}/>
            </Grid>
        })}
    </Grid>
    <Divider />
    <Paper>
        <Box display="flex">
            <IconButton onClick={run} color="primary"><PlayArrowIcon fontSize="large" /></IconButton>
            <JobPoll jobs={jobs} onPoll={async jobs => {
                setJobs(jobs);
                if (jobs[0]?.output_filename)
                    setDownloadLink(await api.files.getFileLink(jobs[0].output_filename));
            }} paused={!jobs.some(x => x.status != "DONE" && x.status != "VALIDATION_ERROR")}/>
            <Box m="auto">
                <Typography variant="overline" component="span">{jobStatus}</Typography>
            </Box>
            <div style={{flexGrow: 1}}></div>
            <Box m="auto" mr={2}>
                <Button disabled={jobs.filter(x => x.status === "DONE").length === 0} color="primary" href={downloadLink}>Download</Button>
            </Box>
        </Box>
    </Paper>
    <Paper className={classes.darker}>
        <Tabs
          value={value}
          indicatorColor="primary"
          textColor="primary"
          onChange={handleChange}
          aria-label="disabled tabs example"
        >
          <Tab label="Results" />
          <Tab label="Log" />
          <Tab label="Timing" />
        </Tabs>
    </Paper>
    <Paper className={classes.output}>
    <FileViewer 
        loading={jobs[0]?.stages ? jobs[0]?.stages[jobs[0]?.stages.length - 1]?.status != "DONE" : false}
        filename={jobs[0]?.output_filename}
        hidden={value !== 0}
    />
    {value == 2 && <StageList 
        jobs={jobs}
        stages={mapping.stages} /> }
    </Paper>
    </>
}));
