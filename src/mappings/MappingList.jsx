import React, { useState, useEffect }from "react";

import Typography from "@material-ui/core/Typography";
import SortableTableWithHeader from "../common/SortableTableWithHeader";

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';

import { withAPIAccess } from "../api/APIAccess";


export default withAPIAccess(function MappingList(props) {
    const { api } = props;

    const [mappings, setMappings] = useState(null);

    useEffect(() => {
        api.mappings.getAll().then(mappings => {
            setMappings(mappings);
        });
    }, []);

    const onClick = e => {

    };

    const deleteMapping = mapping => e => {
        api.mappings.delete(mapping.id).then(e => {
            setMappings(mappings.filter(x => x.id != mapping.id));
        });
    }

    return <SortableTableWithHeader
        headers={[{
            id: "id",
            label: "ID"
        },
        {
            id: "description",
            label: "Description"
        },
        {
            id: "actions",
            label: "Actions"
        }]}
        loading={mappings === null}
        data={mappings || []}
        paginate={true}
        paginationStartRows={5}
        paginationOptions={[5, 10, 20, 50]}
        row={(row, index) => {
            return <TableRow>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.description}</TableCell>
                <TableCell>
                    <Button color="primary" onClick={onClick(row)}>View</Button>
                    <Button color="primary" onClick={deleteMapping(row)}>Delete</Button>
                </TableCell>
            </TableRow>
        }}
        fullWidth
        title="Mappings"
        headerControls={[]}/>
});
