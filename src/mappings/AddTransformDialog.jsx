import React, { useState, useEffect } from "react";

import DialogBox from "../common/DialogBox";

import Select from '@material-ui/core/Select';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Checkbox from '@material-ui/core/Checkbox';

import InputLabel from '@material-ui/core/InputLabel';

import TextField from '@material-ui/core/TextField';

import Button from '@material-ui/core/Button';

export default function AddTransformDialog(props) {
    const { open, headers, handleClose, transform, selectedColumn, action } = props;
    const [values, setValues] = useState(props.values ?? {});
    const [errorDialogOpen, setErrorDialogOpen] = useState(false);

    useEffect(() => {
        setValues(props.values ?? {});
    }, [props.values]);

    useEffect(() => {
        if (!transform || !selectedColumn)
            return;

        const { params } = transform;
        let vals = Object.keys(params).map( x => {
            let param = params[x];

            if (param.src && (!param.inAccumulator || (param.inAccumulator && selectedColumn.keep))) {
                return {
                    [x]: {
                        value: typeof selectedColumn.keepIndex === "undefined" ? selectedColumn.id : selectedColumn.keepIndex,
                        columnHeader: selectedColumn.origName
                    }
                }
            }

            return {[x]: props.values[x]};
        }).filter(x => x).reduce((acc, val) => {
            return {...acc, ...val};
        }, {});

        setValues(vals);

        if (Object.keys(params).some( x => {
            let param = params[x];
            return param.inAccumulator;
        })) {
            if (!selectedColumn.keep) {
                setErrorDialogOpen(true);
                return;
            }
        }

        if (open && !Object.keys(params).some(x => !params[x].src)) {
            handleClose(vals);
        }
    }, [open, transform, selectedColumn]);

    if (!transform)
        return null;

    const params = transform.params;

    const handleChange = name => e => {
        const { value, type, checked } = e.target;
        const columnHeader = selectedColumn.origName;

        if (type == "checkbox") {
            setValues({...values, [name]: {
                columnHeader,
                value: checked
            }});
        } else {
            setValues({...values, [name]: {
                columnHeader,
                value
            }});
        }
    };

    const fields = Object.keys(params).map( x => {
        let param = params[x];
        if (param.type == "column" && !param.src) {
            return <FormControl fullWidth>
                <InputLabel id={x + "-label"}>{param.label}</InputLabel>

                <Select
                  labelId={x + "-label"}
                  id={x}
                  value={values[x]?.value}
                  onChange={handleChange(x)}
                >{ headers.filter(x => (!param.inAccumulator || (param.inAccumulator && x.keep)) && !x.toBeRemoved).map((x, i) => {
                    let id = i;
                    return <MenuItem value={id} >{x.name}</MenuItem>
                }) }
                </Select>
            </FormControl>
        } else if (param.type == "text") {
            return <FormControl fullWidth>
                <TextField fullWidth required id={x} value={values[x]?.value} label={param.label} onChange={handleChange(x)}/>
                </FormControl>
        } else if (param.type == "boolean") {
            return <FormControlLabel
                    control={
                      <Checkbox checked={values[x]?.value} name={x} onChange={handleChange(x)} value={x} />
                    }
                    label={param.label}
                    required
                  />
        }
    }).filter(x => x);

    if (errorDialogOpen) {
        return <DialogBox 
                    title={"Error with transform '" + transform.label + "'"}
                    open={errorDialogOpen}
                    actions={[
                        <Button onClick={e => {
                            handleClose(null);
                            setErrorDialogOpen(false);

                        }}>Close</Button>
                    ]}>
            Please make sure you are keeping this column before you trying adding modifications to it.
        </DialogBox>
    }


    if (fields.length == 0)
        return null;

    return <DialogBox title={`${action || "Add"} transform '${transform.label}'`} actions={
            [<Button onClick={e => handleClose(values)}>{action || "Add"}</Button>,
             <Button onClick={e => handleClose(null)}>Cancel</Button>]
     } {...props}>
     { fields }
    </DialogBox>
}
