import React, { useState, useEffect } from "react";

import SortableTable from "../common/SortableTable";
import SortableTableWithHeader from "../common/SortableTableWithHeader";
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';


import { withAPIAccess } from "../api/APIAccess";

 

const csv = require("neat-csv");

export default withAPIAccess(function TransformList(props) {
    const { transforms, availableTransforms, api, loading, deleteTransform, edit, moveUp, moveDown } = props;

    return <SortableTable
        loading={loading}
        headers={[{
            id: "order",
            label: "Order"
        },
        {
            id: "name",
            label: "Name"
        },
        {
            id: "params",
            label: "Parameters"
        },
        {
            id: "actions",
            label: "Actions"
        }]}
        data={transforms}
        paginate={true}
        paginationStartRows={5}
        paginationOptions={[5, 10, 20, 50]}
        row={(row, index) => {
            let t = availableTransforms.find(x => x.name == row.name);
            console.log(t);
            return <TableRow key={index}>
                <TableCell>{row.order}</TableCell>
                <TableCell>{t.label}</TableCell>
                <TableCell>{Object.keys(t.params).map((x, i) => {
                    return [t.params[x].label + ": " + row.params[x], <br />];
                })}</TableCell>
                <TableCell>
                    <ButtonGroup>
                        <IconButton onClick={moveUp(row)}><ArrowUpward /></IconButton>
                        <IconButton onClick={moveDown(row)}><ArrowDownward /></IconButton>
                        <IconButton onClick={edit(t, row)}><Edit /></IconButton>
                        <IconButton onClick={deleteTransform(row)}><Delete /></IconButton>
                    </ButtonGroup>
                    
                </TableCell>
            </TableRow>
        }} />
});
