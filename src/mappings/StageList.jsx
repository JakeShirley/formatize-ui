import React, { useState, useEffect } from "react";

import SortableTable from "../common/SortableTable";
import SortableTableWithHeader from "../common/SortableTableWithHeader";
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';

import { withAPIAccess } from "../api/APIAccess";

import moment from "moment";

export default withAPIAccess(function StageList(props) {
    const { jobs, stages, api, loading, deleteTransform } = props;

    const jobStatus = jobs[0]?.status;
    const jobStages = jobs[0]?.stages || [];

    return <SortableTable
        loading={loading}
        headers={[{
            id: "stage",
            label: "Stage"
        },
        {
            id: "status",
            label: "Status"
        },
        {
            id: "start",
            label: "Started at"
        },
        {
            id: "end",
            label: "Finished at"
        },
        {
            id: "duration",
            label: "Duration"
        }]}
        data={jobStages.length == stages.length ? stages : []}
        paginate={true}
        minRows={5}
        paginationStartRows={5}
        paginationOptions={[5, 10, 20, 50]}
        row={(row, index) => {
            let job = jobStages.find(x => row.id == x.mapping_stage_id);
            return <TableRow key={index}>
                <TableCell>{row.name}</TableCell>
                <TableCell>{jobStatus === "VALIDATION_ERROR" ? jobStatus : job.status}</TableCell>
                <TableCell>{job.started_at ? moment(job.started_at).format("DD-MM-YYYY HH:mm:ss") : "-"}</TableCell>
                <TableCell>{job.finished_at ? moment(job.finished_at).format("DD-MM-YYYY HH:mm:ss") : "-"}</TableCell>
                <TableCell>{job.started_at && job.finished_at ? moment(new Date(job.finished_at) - new Date(job.started_at)).format("s.SSS") + " s" : "-"}</TableCell>
            </TableRow>
        }} />
});
