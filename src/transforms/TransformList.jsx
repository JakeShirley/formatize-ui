import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';

import red from '@material-ui/core/colors/red';

import SortableTableCRUD from "../common/SortableTableCRUD";
import { withAPIAccess } from "../api/APIAccess";

const TEMPLATE_FUNC = "function TEMPLATE({arg}) {\n" + 
"    //initialisation code here\n" + 
"    return (row, acc) => {\n" + 
"        //main function code here\n"+
"    }\n"+
"}";

function camelize(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
      return index === 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
  }

export default withAPIAccess(function TransformList(props) {
    const { api } = props;
    const [transforms, setTransforms] = useState(null);

    useEffect(() => {
        api.transforms.get().then(transforms => {
            setTransforms(transforms);
        });
    }, []);

    const onDialogAdd = values => {
        api.transforms.create(values.name, values.label, TEMPLATE_FUNC.replace("TEMPLATE", camelize(values.name))).then(transform => {
            setTransforms([...transforms, transform]);
        });
    };

    const deleteTransform = transform => e => {
        api.transforms.delete(transform.name).then(() => {
            setTransforms(transforms.filter(x => x.name !== transform.name));
        });
    };

    return [
    <SortableTableCRUD
        loading={transforms === null}
        headers={[
            {
                id: "label",
                label: "Label"
            },
            {
                id: "description",
                label: "Description"
            },
            {
                id: "actions",
                label: "Actions"
            },]
        }
        data={transforms || []}
        paginate={true}
        paginationStartRows={10}
        paginationOptions={[5, 10, 20, 50]}
        row={(row, index) => {
            return <TableRow>
                <TableCell>{row.label}</TableCell>
                <TableCell>{row.description}</TableCell>
                <TableCell>
                    <Button color="primary" href={"/transform/" + row.name}>View</Button>
                    { !row.locked && <Button color="primary" onClick={deleteTransform(row)}>Delete</Button> }
                </TableCell>
            </TableRow>
        }}
        fullWidth
        title="Transforms"
        onDialogAdd={onDialogAdd}
        dialogTitle="Add transform"
        dialogFields={[
            {
                id: "name",
                label: "Name",
                required: true
            },
            {
                id: "label",
                label: "Label",
                required: true
            }
        ]}
    />];
});
