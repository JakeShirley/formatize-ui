import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';


import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';

import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';

import AceEditor from "react-ace";

import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/mode-json";
import "ace-builds/src-noconflict/mode-html";
import "ace-builds/src-noconflict/theme-clouds";

import Section from "../common/Section";

import { withAPIAccess } from "../api/APIAccess";

import withFileSelector from "../files/FileSelector";
import AppToolbar from "../common/AppToolbar";

import { readLocalFileHeaders } from "../files/Utils.js";

import DialogBoxGenerator from "../common/DialogBoxGenerator";

const useStyles = makeStyles(theme => ({
    info: {
        color: "white",
        fontFamily: theme.typography.button.fontFamily
    }
}));

export default withAPIAccess(withFileSelector(function TransformEditor(props) {
    const { api, fileSelector } = props;

    const { id } = props.match.params;

    const classes = useStyles();

    const [transform, setTransform] = React.useState(null);

    const update = transform => {
        transform.params_json = JSON.stringify(JSON.parse(transform.params_json), null, '\t');
            //console.log(transform);
        setTransform(transform);
    }

    useEffect(() => {
        /*api.transforms.get(id).then(transform => {
            setTransform(transform);
            setHeaders(transform.columns || []);
        });*/

        api.transforms.get(id).then(update);
    }, [id]);

    if (!transform)
        return null;

    const save = e => {
        api.transforms.update(id, transform).then(update);
    };

    return [
        <Section title={"Transform: " + transform.label} fullWidth/>,
        transform && <Section title="Function" headerControls={[
            <Typography variant="button">Valid</Typography>,
            <Button color="inherit" onClick={save}>Save</Button>
        ]}>
            <AceEditor
                mode="javascript"
                theme="clouds"
                name="functions-editor"
                onChange={functions => {
                    //setFunctionsText(functions);
                    setTransform({
                        ...transform,
                        func: functions
                    })
                }}
                value={transform.func}
                minLines={20}
                maxLines={20}
                width="100%"
                editorProps={{ $blockScrolling: true }}
            />
        </Section>,
        transform && <Section title="Parameter metadata" headerControls={[
            <Typography variant="button">Valid</Typography>,
            <Button color="inherit" onClick={save}>Save</Button>
        ]}>
            <AceEditor
                mode="json"
                theme="clouds"
                name="functions-editor"
                onChange={params_json => {
                    //setFunctionsText(functions);
                    setTransform({
                        ...transform,
                        params_json
                    })
                }}
                value={transform.params_json}
                minLines={20}
                maxLines={20}
                width="100%"
                editorProps={{ $blockScrolling: true }}
            />
        </Section>
    ]
}));
