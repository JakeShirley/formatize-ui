import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Box from '@material-ui/core/Box';

import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

import Select from '@material-ui/core/Select';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import SortableTableCRUD from "../common/SortableTableCRUD";

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';

import { withAPIAccess } from "../api/APIAccess";

export default withAPIAccess(function PipelineMappings(props) {
    const [mappings, setMappings] = useState(props.mappings);
    const { api, pipeline } = props;

    const [availableMappings, setAvailableMappings] = useState([]);

    useEffect(() => {
        api.mappings.get().then(mappings => {
            setAvailableMappings(mappings);
        });
    }, [pipeline.id]);

    const onDialogAdd = values => {
        api.pipelines.addMapping(pipeline.id, values.mapping, values.dataSource).then(mapping => {
            setMappings([...mappings, mapping]);
        });
    }

    const deleteMapping = id => e => {
        api.pipelines.deleteMapping(pipeline.id, id).then(e => {
            setMappings(mappings.filter(x => x.mapping.id != id));
        });
    }

    console.log(pipeline);

    return [
            <SortableTableCRUD
                headers={[
                {
                    id: "data_source",
                    label: "Data source"
                },
                {
                    id: "mapping",
                    label: "Mapping"
                },
                {
                    id: "mapping_description",
                    label: "Mapping Description"
                },
                {
                    id: "actions",
                    label: "Actions"
                },]}
                data={mappings}
                row={(row, index) => {
                    return <TableRow>
                        <TableCell>{row.data_source.name}</TableCell>
                        <TableCell>{row.mapping.id}</TableCell>
                        <TableCell>{row.mapping.description}</TableCell>
                        <TableCell>
                            <Button href={"/mapping/" + row.mapping.id}>View</Button>
                            <Button onClick={deleteMapping(row.mapping.id)}>Detach</Button>
                        </TableCell>
                    </TableRow>
                }}
                title="Data mappings"
                dialogTitle="Add mapping"
                onDialogAdd={onDialogAdd}
                dialogFields={[
                    {
                        id: "dataSource",
                        label: "Data source",
                        required: true,
                        type: "select",
                        data: pipeline.data_sources.map(x => ({id: x.id, label: x.name}))
                    },
                    {
                        id: "mapping",
                        label: "Mapping",
                        required: true,
                        type: "select",
                        data: availableMappings.map(x => ({id: x.id, label: x.id}))
                    },
                ]}
            />
    ];
});
