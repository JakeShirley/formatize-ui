import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Box from '@material-ui/core/Box';

import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

import Select from '@material-ui/core/Select';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import SortableTableCRUD from "../common/SortableTableCRUD";

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';

import DataSourceRow from "./DataSourceRow";

import { withAPIAccess } from "../api/APIAccess";

import DialogBox from "../common/DialogBox";

export default withAPIAccess(function DataSources(props) {
    const [sources, setSources] = useState(props.sources);
    const { api, pipeline } = props;

    const onDialogAdd = values => {
        api.pipelines.addDataSource(pipeline.id, values.id, values.name, values.type).then(source => {
            let s = [...sources, source];

            console.log(source);

            setSources(s);
            if (props.onUpdate)
                props.onUpdate(s);
        }).catch(res => {
        });
    }

    const update = id => values => {
        api.pipelines.updateDataSource(pipeline.id, id, values.id, values.name, values.type).then(res => {
            let s = sources.map(x => {
                if (x.id == id) {
                    return {...x, ...values};
                } else {
                    return x;
                }
            });

            setSources(s);
            if (props.onUpdate)
                props.onUpdate(s);
        });
    }

    return [<SortableTableCRUD
                headers={[{
                    id: "id",
                    label: "ID"
                },{
                    id: "name",
                    label: "Name"
                },
                {
                    id: "type",
                    label: "Type"
                },
                {
                    id: "description",
                    label: "Description"
                },
                {
                    id: "actions",
                    label: "Actions"
                },]}
                data={sources}
                row={(row, index) => {
                    return <DataSourceRow source={row} onUpdate={update(row.id)}/>
                }}
                title="Data sources"
                dialogTitle="Add data source"
                onDialogAdd={onDialogAdd}
                dialogFields={[
                    {
                        id: "id",
                        label: "ID",
                        required: true
                    },
                    {
                        id: "name",
                        label: "Name",
                        required: true
                    },
                    {
                        id: "type",
                        label: "Type",
                        required: true,
                        type: "select",
                        data: [
                            {
                                id: "upload",
                                label: "Upload"
                            },
                            {
                                id: "api-connection",
                                label: "API Connection"
                            }
                        ]
                    },
                ]}
            />
    ];
});
