import React, { useState, useEffect } from "react";

import AppToolbar from "../common/AppToolbar";

import DataSources from "./DataSources";
import PipelineMappings from "./PipelineMappings";

import { withAPIAccess } from "../api/APIAccess";

export default withAPIAccess(function Pipeline(props) {
    const { api } = props;
    const { id } = props.match.params;

    const [pipeline, setPipeline] = useState(null);

    useEffect(() => {
        api.pipelines.get(id).then(pipeline => {
            setPipeline(pipeline);
        });
    }, [id]);

    if (!pipeline)
        return null;

    console.log(pipeline);

    return [
        <AppToolbar title={pipeline.name} />,
        <DataSources pipeline={pipeline} sources={pipeline.data_sources} onUpdate={sources => {
                console.log(sources);
            setPipeline({...pipeline, data_sources: sources});
        }}/>,
        <PipelineMappings pipeline={pipeline} mappings={pipeline.mapping_list} />
    ];
});
