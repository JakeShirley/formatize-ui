import React, { useState, useEffect }from "react";

import Typography from "@material-ui/core/Typography";
import SortableTableCRUD from "../common/SortableTableCRUD";

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';

import { withAPIAccess } from "../api/APIAccess";


export default withAPIAccess(function PipelineList(props) {
    const { api } = props;

    const [pipelines, setPipelines] = useState(null);

    useEffect(() => {
        api.pipelines.get().then(pipelines => {
            setPipelines(pipelines);
        });
    }, []);

    const onDialogAdd = values => {
        api.pipelines.create(values.name).then(pipeline => {
            setPipelines([...pipelines, pipeline]);
        });
    };

    const view = e => {

    };

    const deletePipeline = pipeline => e => {
        api.pipelines.delete(pipeline.id).then(e => {
            setPipelines(pipelines.filter(x => x.id != pipeline.id));
        });
    }

    return <SortableTableCRUD
        headers={[{
            id: "name",
            label: "Name"
        },
        {
            id: "description",
            label: "Description"
        },
        {
            id: "actions",
            label: "Actions"
        }]}
        loading={pipelines === null}
        data={pipelines || []}
        paginate={true}
        paginationStartRows={5}
        paginationOptions={[5, 10, 20, 50]}
        row={(row, index) => {
            return <TableRow>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.description}</TableCell>
                <TableCell>
                    <Button href={"/pipeline/" + row.id} color="primary">View</Button>
                    <Button color="primary" onClick={deletePipeline(row)}>Delete</Button>
                </TableCell>
            </TableRow>
        }}
        fullWidth
        title="Pipelines"
        onDialogAdd={onDialogAdd}
        dialogTitle="Add pipeline"
        dialogFields={[
            {
                id: "name",
                label: "Name",
                required: true
            }
        ]}/>
});
