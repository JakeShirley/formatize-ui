import React, { useState, useEffect } from "react";

import { makeStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

import Select from '@material-ui/core/Select';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Button from '@material-ui/core/Button';


const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        margin: theme.spacing(2)
    },
}));

const TYPES = [
    {
        id: "upload",
        label: "Upload"
    },
    {
        id: "api-connection",
        label: "API Connection"
    },
    {
        id: null,
        label: "Unknown"
    }
]

export default function DataSourceRow(props) {
    const classes = useStyles();

    const { source } = props;

    const [edit, setEdit] = useState(false);
    const [values, setValues] = useState(source || {});

    console.log(source);

    const handleChange = name => e => {
        const {name, value} = e.target;
        setValues({...values, [name] : value})
    };

    const editHandler = e => {
        if (edit) {
            props.onUpdate(values);
        }

        setEdit(!edit);
    };

    return <TableRow>
        <TableCell>
            { !edit ?
                source.id :
                <TextField fullWidth label="Id" name="id" value={values["id"]} onChange={handleChange("id")}/>
            }
        </TableCell>
        <TableCell>
            { !edit ?
                source.name:
                <TextField fullWidth label="Name" value={values["name"]} onChange={handleChange("name")}/>
            }
        </TableCell>
        <TableCell>
        { !edit ?
            TYPES.find(x => x.id === source.type).label :
            <FormControl fullWidth>
                <InputLabel id={"type-label"}>Type</InputLabel>

                <Select
                  labelId={"type-label"}
                  id="type"
                  value={values["type"]}
                  onChange={handleChange("type")}
                >{ TYPES.map(x => {
                    return <MenuItem value={x.id}>{x.label}</MenuItem>
                }) }
                </Select>
            </FormControl>}
        </TableCell>
        <TableCell>
            { !edit ?
                source.description :
                <TextField fullWidth label="Description" value={values["description"]} onChange={handleChange("description")}/>}
        </TableCell>
        <TableCell>
            <Button onClick={editHandler}>Edit</Button>
        </TableCell>
    </TableRow>

    /*return <Grid container spacing={3}>
                <Grid item xs={3}>
                    { !edit ? <Typography variant="overline" component="h3">{source.name}</Typography> :
                    <TextField fullWidth label="Name" />}
                </Grid>
                <Grid item xs={3}>

                </Grid>
                <Typography variant="overline" component="h3">{source.description}</Typography>
                <Grid item xs={6}>
                    <TextField fullWidth label="Description" />
                </Grid>
            </Grid>*/
}
